
Rubber Admin, Admin Area Interface
-----------------------------------------

Installation
-----------------------------------------

**Add the package to the composer.json**

````
"require": {
	....
	"rubber/admin": "*"
}
````

**Add repositories section to composer.json**

````
"repositories": [
	{
		"type": "vcs",
		"url": "git@bitbucket.org:darkpuresnow/rubber-admin.git"
	}
],
````

Then run composer update to get the packages.


**Adding service provider**

````
'providers' => array(
	....
	'Rubber\Admin\AdminServiceProvider',
	'Baum\BaumServiceProvider',
	'Thomaswelton\LaravelGravatar\LaravelGravatarServiceProvider',
	'Cartalyst\Sentry\SentryServiceProvider',
	'Intervention\Image\ImageServiceProvider',
}
````

**Adding class alias / Facades**

Also, in the app.php, add these lines into the alias array:

````
'aliases' => array(
	....
	'Sentry' => 'Cartalyst\Sentry\Facades\Laravel\Sentry',
	'Tinker' => 'Rubber\Admin\Support\Facades\FoundationFormBuilder',
	'Image'=> 'Intervention\Image\Facades\Image',
	'Gravatar' => 'Thomaswelton\LaravelGravatar\Facades\Gravatar',
	'Chronicle' =>  'Rubber\Admin\Support\Facades\UserHistoryWriter'
}
````

Installing the package
-----------------------------------------

After including the above 2 important points into app.php, you can run this command to install the whole package.

````
php artisan rubber/admin:install
````

This command will do a batch command for you:
- Run the database migration for Cartalyst/Sentry
- Run the database migration for Rubber/Admin
- Publishing config files for these related packages
	- Cartalyst/Sentry
	- Rubber/Admin
	- Invervention/Image
	- thomaswelton/laravel-gravatar
- Publish required asset files to the public folder

Installing Manually
-----------------------------------------

In this version, the migration would failed if you have specific app environment setup. In this case, please manually run this 2 migration in the command line:
````
php artisan migrate --env=[you-environment] --package=cartalyst/sentry
php artisan migrate --env=[you-environment] --package=rubber/admin
````

Also, you could run this command for running only the migration:

````
php artisan rubber/admin:migrate
````

The same command is also available for publishing configuration files:

````
php artisan rubber/admin:config
````

And one command for publhing assets:

````
php artisan rubber/admin:assets
````

More Configuration
-----------------------------------------

After executing the above command lines,  go to your app/config/package/cartalyst/sentry/config.php, change the groups configuration to these following lines:

````
'groups' => array(
	'model' => 'Rubber\Admin\Group',
),
````

And change the users configuration to these:

````
'users' => array(
	'model' => 'Rubber\Admin\User',
	'login_attribute' => 'email'
),
````


Initial user setup
-----------------------------------------

The last step for installation is to create the first admin account to access to system. Navigate to this url for adding the first super user information. (Please note that the page will be accessible only if there is NO DATA on the user table).


````
http://domain.com/admin/install
````

Enter the credential for the user, submit the data, then you are ready to login to the backend.

The basic backend structure
-----------------------------------------

The package come with this functionality:
- Navigation Menu Management
- User Accounts and Permission Management

Navigation Menu Management
-----------------------------------------

You could create the main navigation bar by using this modules. You may either:
- Click on the green button at the bottom "Add this page to menu" for quickly added the current page to the top bar. This link is available only if the current user has access to 'admin.navigation.store' route.
- Use the navigation form to create a new menu item.

The basic of the navigation menu
-----------------------------------------

The navigation menu currently built upon the Baum\Node package, which allow unlimited level of nested tree. At the mean time, there could be only 1 navigation tree in the system, which mean there is no support for having a different menu for different user groups yet.

A navigation menu item can be differentiate into 2 types:
- A "fixed" link which pointed to a given url. This type requires a completed URL.
- A "named route" link which uses the Laravel's named route for translating into a real URL. The module will grab all the named routes defined in the routes.php, or whereever files registered to the system, and generate it in a dropdown to choose from.

Some of  "named route" registered to the system might not appear in the dropdown. The reason is that the code will filter out any routes not accepting GET method since there is no way to access the non-GET route directly from the browser url Also any route with binded parameter will not be included.

Also, any route with binded parameter, eg. /user/{id}, will not be usable on the menu since there is no way for the navigation builder to know what variables, and in which order, should be included during the url generation. If this kind of route is required, It'd be better to use the fixed url insteaSome of  "named route" registered to the system might not appear in the dropdown. The reason is that the code will filter out any routes not accepting GET method since there is no way to access the non-GET route directly from the browser url Also any route with binded parameter will not be included.

Before journey on building the system, It's highly recommended to add these few named routes  in to the navigation bar:
- admin.user.list : List of users added into the database
- admin.group.list : List of available user group
- admin.navigation.list : List of navigation links added to the database.

User Accounts and Permission Management
-----------------------------------------

The package is using Cartalyst\Sentry as a main engine for the authorization and user management, with some system built up on it for easier managing of permissions. You may visit [the Sentry documentation](https://cartalyst.com/manual/sentry) if you'd like to do some extra code related to user authentication.

After installation, there will be 1 super user, and 1 user group, with a super user permission in the database. You may added as many users and user groups as you want, using the User Accounts and Permission Management.

By default, the first created user during installation will have a permission of "superadmin", which allow access on any routes without restriction.

The Users modules consist of 3 main parts:
- Users management
- Groups management
- Permission management

As of the current version (1.0), Rubber/Admin package will apply the permission cheking on any routes with a filter "adminAuth". So if you have created a page / section, and wanted to prevent access to outsider, you can put the route under the filter like this:

````
<?php
	Route::get('test/filter', array(
		'as' => 'your-route-name', // required if you need to check with per-page-permission,
		'uses' => 'yourcontroller@method'
	))->before('adminAuth');
?>
````

OR, use a route group to apply filter on multiple routes.

````
<?php
Route::group(
	array(
		'before' => 'adminAuth'
	),
	function(){
		...
		Route::get('test/filter', array(
			'as' => 'your-route-name', // required if you need to check with per-page-permission,
			'uses' => 'yourcontroller@method'
		))
		...
	}
});

?>
````

**NOTE**
The current version (1.0.5.x) doesn't fully support having multiple filter apply to a route. With the above example, if the route has a filter of "adminAuth|something-else", it will not appear in the available Permission list since the system is looking for routes with an exact filter of "adminAuth"

If a user has been added to the system, he/she could login to the system, but has no rights to access any pages yet. The user must be added into one or more groups, then he/she will have the right to access pages assigned to those groups.

By default, a group has no access to any pages. You'd have to visit the permissions of the group, and enable the part you'd like to grant access to the group.

To summarize with example:

User A will be able to access anything starting with page, page/1, page/3/delete, page/search. In this case, a group named "Page Admin" should be created. Then this group will be granted access to those url above.

Also, User A will be able to access "reports" modules. Maybe a "Report Manager" could be added, with the said permissions.

After that, the user A will be added to both "Page admin" and "Report Manager" group, which will allow him  to access both page/* and reports/*.


**Overriding Package Language File**
-----------------------------------------

As taken from Laravel documentaion:

> Many packages ship with their own language lines. Instead of hacking the package's core files to tweak these lines, you may override them by placing files in the **app/lang/packages/{locale** directory. So, for example, if you need to override the English language lines for a package named **skyrim/hearthfire**, you would place a language file at: **app/lang/packages/en/hearthfire.php**. In this file you would define only the language lines you wish to override. Any language lines you don't override will still be loaded from the package's language files.
 &mdash; <cite>[Laravel Documenttion : Localization][1]</cite>

[1]:http://laravel.com/docs/localization

Hence, you can copy the package language folder, located in **vendor/rubber/admin/src/lang** into your **app/lang/packages/admin** folder, then edit the file content as you wish.

However, there is no language file for this package yet.

##Something Else##
-----------------


Bon Voyage.