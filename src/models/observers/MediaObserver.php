<?php namespace Rubber\Admin\Observer;
	class MediaObserver extends BaseObserver{

		public function saving($model)
		{
			return $model->validate();
		}

		public function deleted($model){
			$model->removeSrcFile();
			return;
		}

	}
	/** End of file : app/models/observer/UserObserver.php