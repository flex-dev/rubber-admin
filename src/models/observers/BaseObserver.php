<?php namespace Rubber\Admin\Observer;
	class BaseObserver{
		public function saving($model)
		{
			return $model->validate();
		}
	}
?>