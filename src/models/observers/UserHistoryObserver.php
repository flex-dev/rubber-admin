<?php namespace Rubber\Admin\Observer;
	class UserHistoryObserver {

		public function creating($model){
			$model->user_id = \Sentry::getUser()->id;
			return true;
		}

	}
	/** End of file : app/models/observer/UserObserver.php