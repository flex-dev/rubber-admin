<?php namespace Rubber\Admin\Observer;
	class UserObserver extends BaseObserver{

		public function saving($model)
		{
			return $model->validate();
		}

		public function created($model)
		{

		}

		public function updated($model)
		{

		}

		public function deleted($model){

		}

	}
	/** End of file : app/models/observer/UserObserver.php