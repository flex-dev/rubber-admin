<?php namespace Rubber\Admin\Observer;
	class UserAccessObserver {

		public function creating($model){
			$model->ip = \Request::getClientIp();
			$model->user_agent = \Request::server('HTTP_USER_AGENT');
			$model->user_id = \Sentry::getUser()->id;
			return true;
		}

	}
	/** End of file : app/models/observer/UserObserver.php