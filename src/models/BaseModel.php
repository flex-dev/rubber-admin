<?php namespace Rubber\Admin;

/**
 * A base model containing common methods and properties.
 */
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Input;
use Config;
use Event;
use Html;

abstract class BaseModel extends Model
{

    protected $guarded = [];

    public static $rules = [];

    public static $messages = [];

    protected $path = [];

    protected $validator = null;

    protected static $errors = [];

    protected static $sortOptions = [
        'sort'      => 'id',
        'direction' => 'desc'
    ];

    protected static $observer = 'Rubber\Admin\Observer\BaseObserver';


    /**
     * Listen for save event,  idea taken from Aidkit package
     */
    protected static function boot()
    {
        parent::boot();
    }


    /**
     * Extend unique rules to be automatically apply model id to the rules
     * See also: https://gist.github.com/JonoB/6637861
     *
     * @param  array $input [Input to be validated]
     *
     * @return [Boolean]  true / false based on the validation results
     */
    public function validate($input = [])
    {
        $replace = ( $this->getKey() > 0 )
            ? $this->getKey()
            : '';
        foreach (static::$rules as $key => $rule) {
            static::$rules[$key] = str_replace(':id', $replace, $rule);
        }

        $validator = $this->validator->make($this->attributes, static::$rules, static::$messages);

        if ($validator->passes()) {
            return true;
        }

        $this->setErrors($validator->messages());

        return false;
    }


    /**
     * Set error message bag
     *
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }


    /**
     * Retrieve error message bag
     */
    public function getErrors()
    {
        return $this->errors;
    }


    /**
     * Return if there are any errors
     *
     * @return bool
     */
    public function hasErrors()
    {
        return ! empty( $this->errors );
    }


    /**
     * Basic filtering for showing trashed / non-trashed
     *
     * @param  [type] $query  [description]
     * @param  [type] $filter [description]
     *
     * @return [type]         [description]
     */
    public function scopeApplyFilter($query, $filter = null)
    {
        if (is_array($filter)) {
            if (isset( $filter['trashed'] )) {
                return $query->onlyTrashed();
            }
            if (isset( $filter['all'] )) {
                return $query->withTrashed();
            }
        }
        switch ($filter) {
            case 'trashed':
                $query->onlyTrashed();
                break;
            case 'all':
                $query->withTrashed();
                break;
            default:
                break;
        }

        return $query;
    }


    /**
     * Apply sort and sort direction as specify in the query string
     * sorting field and direction determined by the var included on via GET or POST
     *
     * @param        $query
     * @param string $defaultSort
     * @param string $defaultDirection
     *
     * @return   [QueryBuilder] $query
     */
    public function scopeApplySort($query, $defaultSort = 'id', $defaultDirection = 'asc')
    {
        $direction = Input::get('direction', $defaultDirection);

        $sort = Input::get('sort', $defaultSort);
        if (empty( $sort )) {
            $sort = 'id';
        }
        static::$sortOptions['sort']      = $sort;
        static::$sortOptions['direction'] = $direction;

        return $query->orderBy($sort, $direction);
    }


    // Return the current model sorting, the applySort() must be called to get a good value
    public function getSort()
    {
        return $this->sortOptions;
    }


    /**
     * Return the path to a folder specify inside the model.
     *
     * @param $folder
     *
     * @return   [type]         [description]
     * @throws Exception\PathNotFoundException
     */
    public function getPath($folder)
    {
        if (empty( $this->path[$folder] )) {
            throw new Exception\PathNotFoundException('The path ' . $folder . ' is not defined in the ' . get_class($this));
        }

        return $this->path[$folder];
    }


    public function getStatusTextAttribute()
    {
        if ( ! isset( $this->attributes['status'] )) {
            return 'n/a';
        }

        return $this->attributes['status']
            ? 'Enabled'
            : 'Disabled';
    }

    // reload the model data from the database, or just use the original value if the data has been deleted.
    // Taken from Baum\Baum model
    public function reload()
    {
        if ( ! $this->exists) {
            $this->syncOriginal();
        } else {
            $fresh = static::find($this->getKey());

            $this->setRawAttributes($fresh->getAttributes(), true);
        }

        return $this;
    }


    public function getStatusLabelAttribute()
    {
        if ($this->trashed()) {
            $deleted = 'title="deleted at ' . $this->deleted_at . '"';

            return $this->labelError($deleted);
        }
        if ($this->attributes['visibility'] == 1) {
            return $this->labelSuccess();
        } else {
            $icon = $this->icon('fa-times fa-lg error');

            return $this->label(null, $icon);
        }
    }


    protected function icon($icon)
    {
        return Html::icon($icon);
    }


    protected function label($type = null, $content = null, $attributes = [])
    {
        if (empty( $type )) {
            $type = 'secondary circular';
        }
        if (is_array($attributes)) {
            $attributes = Str::assocToAttr($attributes);
        }
        $pattern = '<span class="label %s" %s>%s</span>';

        return sprintf($pattern, $type, $attributes, $content);
    }


    public function labelError($attributes = null)
    {
        return $this->label('alert circular', $this->icon('fa-trash-o fa-lg error'), $attributes);
    }


    public function labelSuccess($attributes = null)
    {
        return $this->label('success circular', $this->icon('fa-check fa-lg'), $attributes);
    }


    protected function getLikePattern($wrapping = 'both')
    {
        switch (strtolower($wrapping)) {
            case 'left':
                $pattern = "%%%s";
                break;
            case 'right':
                $pattern = "%s%%";
                break;
            case 'both':
                $pattern = "%%%s%%";
                break;
            default:
                throw new InvalidArgumentException('The wrapping value for LIKE condition must be either `left`, `right` or `both`');
        }

        return $pattern;
    }


    public function scopeWhereLike($query, $field, $value, $wrapping = 'both')
    {
        $pattern = self::getLikePattern($wrapping);

        return $query->where($field, 'LIKE', sprintf($pattern, $value));
    }


    public function scopeOrWhereLike($query, $field, $value, $wrapping = 'both')
    {
        $pattern = self::getLikePattern($wrapping);

        return $query->orWhere($field, 'LIKE', sprintf($pattern, $value));
    }


    protected function tooltip($text)
    {
        $tooltip = [
            'title'        => $text,
            'data-tooltip' => '',
            'class'        => 'has-tip tip-top',
        ];

        return $tooltip;
    }


    public function scopeByDate($query, $start = null, $end = null)
    {
        switch (true) {
            case ! empty( $start ) && ! empty( $end ):
                $query->whereBetween('created_at', [
                    $start,
                    $end
                ]);
                break;
            case ! empty( $start ):
                $query->where('created_at', '>=', $start);
                break;
            case ! empty( $end ):
                $query->where('created_at', '<=', $end);
                break;
            default;
        }
    }

}
