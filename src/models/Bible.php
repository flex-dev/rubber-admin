<?php namespace Rubber\Admin;

use Rubber\Admin\BaseModel;
use Route;
use StdClass;
use Str;

class Bible extends BaseModel
{

	protected $table = 'rubber_bibles';

	public $fillable = [
		'name',
		'description',
		'route',
		'method'
	];

	public static $rules = [
		'name'        => 'max:255',
		'description' => 'max:255',
		'route'       => 'max:255',
		'method'      => ''
	];


	public function getUpdateUrlAttribute()
	{
		return route('admin.bible.update', $this->id);
	}


	static public function getRoutes()
	{
		$definedRoutes = Route::getRoutes();
		$routes        = [];
		foreach ($definedRoutes as &$route) {
			// remove route without any filter from the list, since they will not be checked against adminAuth filter anyway.
			$action = $route->getAction();

			if (empty( $action['before'] )) {
				continue;
			}
			if (is_array($action['before'])) {
				if (in_array('adminAuth', $action['before'])) {
					continue;
				}
			} else {
				if ( ! Str::contains($action['before'], 'adminAuth')) {
					continue;
				}
			}

			$router          = new StdClass;
			$router->methods = $route->getMethods();
			$router->path    = $route->getPath();
			$router->action  = $action;

			if ( ! empty( $router->action['as'] )) { // The as is available only for named routes
				$router->name = $router->action['as'];
			} else { // use URI as the route name if the above one not exists
				$router->name = $router->path;
			}

			if ( ! empty( $router->action['controller'] )) {
				$callback           = Str::parseCallback($router->action['controller'], '');
				$router->function   = array_pop($callback);
				$class              = explode('\\', current($callback));
				$router->controller = array_pop($class);
				$package            = implode('\\', $class);
				if (strpos($router->action['controller'], '\\') === false) {
					$package = '[Global Namespace]';
				}
				$router->package = $package;
				$router->route   = $route;

			} else {
				$package            = '[Global Namespace]';
				$router->package    = $package;
				$router->controller = '[Closure]';
				$router->function   = '[Closure]';
				$router->route      = $route;
			}
			$routes[$package][$router->controller][] = $router;
		}

		return $routes;
	}
}
