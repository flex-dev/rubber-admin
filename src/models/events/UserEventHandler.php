<?php namespace Rubber\Admin\Event;
	use Request;
	use Chronicle;
	class UserEventHandler {

		/**
		 * Handle user login events.
		 */
		public function onUserLogin($event)
		{
		    	$access = new \Rubber\Admin\UserAccess();
			$access->save();

			Chronicle::write('Login success');

			return;
		}

		/**
		 * Handle user logout events.
		 */
		public function onUserLogout($event)
		{
		    	Chronicle::write('user logout');
		}

		/**
		 * Register the listeners for the subscriber.
		 *
		 * @param  Illuminate\Events\Dispatcher  $events
		 * @return array
		 */
		public function subscribe($events)
		{
		    $events->listen('rubber/admin::auth.login', 'Rubber\Admin\Event\UserEventHandler@onUserLogin');
		    $events->listen('rubber/admin::auth.logout', 'Rubber\Admin\Event\UserEventHandler@onUserLogout');
		}

	}

?>