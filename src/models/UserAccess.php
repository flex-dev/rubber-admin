<?php namespace Rubber\Admin;

class UserAccess extends BaseModel
{

	protected $guarded = [];

	protected $table = 'user_access_logs';

	public static $rules = [];


	public static function boot()
	{
		parent::boot();
		static::observe(new Observer\UserAccessObserver);
	}


	public function user()
	{
		return $this->belongsTo('Rubber\Admin\User', 'user_id');
	}


	public function history()
	{
		return $this->hasMany('Rubber\Admin\UserHistory', 'access_id');
	}

}
