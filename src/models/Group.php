<?php namespace Rubber\Admin;

use Cartalyst\Sentry\Groups\Eloquent\Group as SentryGroup;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Group extends SentryGroup
{



    public function getUrlUserByGroupAttribute()
    {
        return route('admin.user.bygroup', $this->attributes['name']);
    }


    public function getNameAnddescAttribute()
    {
        return $this->attributes['name'] . ' - ' . $this->attributes['description'];
    }


    public function getShowUrlAttribute()
    {
        return route('admin.group.show', $this->id);
    }


    public function getEditUrlAttribute()
    {
        return route('admin.group.edit', $this->id);
    }


    public function getDeleteUrlAttribute()
    {
        return route('admin.group.delete', $this->id);
    }


    public function getRestoreUrlAttribute()
    {
        return route('admin.group.restore', $this->id);
    }


    public function superuser()
    {
        return $this->hasAccess('superuser');
    }
}
