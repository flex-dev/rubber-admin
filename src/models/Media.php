<?php namespace Rubber\Admin;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

class Media extends BaseModel
{

	protected $guarded = [];

	public static $rules = [
		'src' => 'required'
	];

	protected $path = [
		'src' => 'medias/'
	];

	protected $appends = [
		'link'
	];

	protected $table = 'rubber_medias';

	protected static $observer = 'Rubber\Admin\Observer\MediaObserver';


	public function setSrcAttribute(UploadedFile $file = null)
	{
		if (empty( $file )) {
			return;
		}
		if ($file instanceof UploadedFile) {
			$path     = $this->getPath('src');
			$filename = sprintf('image-%s.%s', time(), $file->getClientOriginalExtension());
			$file->move($path, $filename);
			$this->attributes['src'] = $filename;
		}

		return;
	}


	public function getSrcPathAttribute()
	{
		return $this->getPath('src') . $this->attributes['src'];
	}


	public function getLinkAttribute()
	{
		$src = $this->getSrcPathAttribute();

		return asset($src);
	}


	public function removeSrcFile()
	{
		if ( ! empty( $this->attributes['src'] )) {
			$file = $this->getSrcPathAttribute();
			if (File::exists($file)) {
				File::delete($file);
			}
		}

		return;
	}
}
