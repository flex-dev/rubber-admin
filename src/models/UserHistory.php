<?php namespace Rubber\Admin;

class UserHistory extends BaseModel
{

	protected $guarded = [];

	protected $table = 'user_history';

	public static $rules = [];


	public static function boot()
	{
		parent::boot();
		static::observe(new Observer\UserHistoryObserver);
	}


	public function user()
	{
		return $this->belongsTo('Rubber\Admin\User', 'user_id');
	}


	public function access()
	{
		return $this->belongsTo('Rubber\Admin\UserAccess', 'access_id');
	}


	public function dataExists()
	{
		return ! empty( $this->attributes['data'] );
	}


	public function download()
	{

	}


	public function getDownloadUrlAttribute()
	{
		return route('admin.history.download', $this->attributes['id']);
	}


	public function getFileNameAttribute()
	{
		return 'history-' . $this->attributes['id'] . '.json';
	}

}
