<?php namespace Rubber\Admin;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Navigation extends HierarchyBaseModel
{
	use SoftDeletingTrait;

	protected $table = 'rubber_navigations';

	public $fillable = [
		'name',
		'description',
		'icon',
		'route_name',
		'route_to'
	];

	public static $rules = [
		'name'       => 'required',
		'parent_id'  => 'required|exists:rubber_navigations,id',
		'route_name' => 'required_without:route_to',
		'route_to'   => 'required_without:route_name'
	];

	public static $messages = [
		'name'       => [
			'required' => 'Please specify a menu name'
		],
		'parent_id'  => [
			'required' => 'Please choose a parent menu, or select the root item if the menu is on the top level',
			'exists'   => 'The selected parent menu with id [:value] does not exists'
		],
		'route_name' => 'Please specify a route name for this menu, or specify a url below',
		'route_to'   => 'Please specify a url if a route name is not specify'
	];

	// 'parent_id' column name
	protected $parentColumn = 'parent_id';

	// 'lft' column name
	protected $leftColumn = 'lft';

	// 'rgt' column name
	protected $rightColumn = 'rght';

	// 'depth' column name
	protected $depthColumn = 'depth';

	// guard attributes from mass-assignment
	protected $guarded = [
		'id',
		'parent_id',
		'lft',
		'rght',
		'depth'
	];


	public function depth($repeator)
	{
		return str_repeat($repeator . ' ', $this->attributes['depth']);
	}


	public function parent()
	{
		return $this->belongsTo('Rubber\Admin\Navigation', 'parent_id');
	}


	public function getNameWithIconAttribute()
	{
		$name = $this->attributes['name'];
		$name = $this->getIconTagAttribute() . ' ' . $name;

		return $name;
	}


	public function getIconTagAttribute()
	{
		if ( ! empty( $this->attributes['icon'] )) {
			return \Html::icon($this->attributes['icon']);
		}

		return false;
	}


	public function getIntendedRouteAttribute()
	{
		return empty( $this->attributes['route_to'] )
			? $this->attributes['route_name']
			: $this->attributes['route_to'];
	}

}
