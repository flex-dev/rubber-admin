<?php namespace Rubber\Admin;

Interface RubberModelInterface
{

    public function validate($input);


    public function getErrors();


    public function hasErrors();


    public function scopeApplySort($query, $defaultSort = null, $defaultDirection = 'asc');


    public function scopeApplyFilter($query, $filter = null);


    public function getSort();


    public function getPath($folder);


    public function getStatusTextAttribute();


    public function getMissingImageAttribute();


    public function reload();

}

?>