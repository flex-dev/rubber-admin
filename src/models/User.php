<?php namespace Rubber\Admin;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Gravatar;
use Sentry;
use Html;
use Str;
use Image;

class User extends \Cartalyst\Sentry\Users\Eloquent\User
{

	public static $rules = [
		'email'      => 'required|unique:users|email',
		'password'   => 'min:5|confirmed',
		'first_name' => 'required',
		'avatar'     => 'image|max:300'
	];

	public static $messages = [
		'email.required'      => 'Please enter a valid email adress.',
		'email.email'         => 'Please enter a valid email adress.',
		'email.unique'        => 'The email has already been used by another user.',
		'password.min'        => 'The password must be at least :min characters',
		'password.confirmed'  => 'The password did not matched.',
		'first_name.required' => 'Please enter the first name of this user',

	];

	protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'avatar',
		'password',
		'activated',
		'permissions'
	];

	protected $path = [
		'avatar' => 'packages/rubber/admin/avatars/'
	];

	protected static $observer = 'Rubber\Admin\Observer\UserObserver';


	protected static function boot()
	{
		parent::boot();
		self::observe(new self::$observer);
	}


	/**
	 * Return the path to a folder specify inside the model.
	 *
	 * @param $folder
	 *
	 * @return   [type]         [description]
	 * @throws Exception\PathInvalidException
	 * @throws Exception\PathNotFoundException
	 *
	 * @todo     Code copy from Rubber\Admin\BaseModel since we cannot extends the base model here. Maybe upgrade it when
	 *         possible with Traits ?
	 */
	public function getPath($folder)
	{
		if (empty( $folder )) {
			throw new Exception\PathInvalidException('The path ' . $folder . ' is not defined in the ' . get_class($this));
		}
		if (empty( $this->path[$folder] )) {
			throw new Exception\PathNotFoundException('The path ' . $folder . ' is not defined in the ' . get_class($this));
		}

		return $this->path[$folder];
	}


	public function getDates()
	{
		return [
			'created_at',
			'updated_at',
			'last_login'
		];
	}


	public function access()
	{
		return $this->hasMany('Rubber\Admin\UserAccess', 'user_id');
	}


	public function history()
	{
		return $this->hasMany('Rubber\Admin\UserHistory', 'user_id');
	}


	public function notifications()
	{
		return $this->hasMany('Rubber\Admin\User\Notification', 'user_id');
	}


	public function outgoing()
	{
		return $this->hasMany('Rubber\Admin\User\Notification', 'sender_id');
	}


	public function getFullNameAttribute()
	{
		$name = $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
		$name = trim($name);

		return $name;
	}


	public function getFullNameOrEmailAttribute()
	{
		$name = $this->getFullNameAttribute();
		if (empty( $name )) {
			return $this->attributes['email'];
		}

		return $name;
	}


	public function getFullNameAndEmailAttribute()
	{
		return $this->getFullNameAttribute() . ' <' . $this->attributes['email'] . '>';
	}


	public function getActivatedTextAttribute()
	{
		return $this->attributes['activated']
			? 'Activated'
			: 'Pending';
	}


	// An allias accessor method for getAvatarImagePathAttribute().
	public function getAvatarImageSrcAttribute()
	{
		if (empty( $this->attributes['avatar'] )) {
			return Gravatar::src($this->attributes['email']);
		} else {
			return asset($this->getAvatarSrcAttribute());
		}
	}


	public function getAvatarImagePathAttribute()
	{
		if (empty( $this->attributes['avatar'] )) {
			return Gravatar::src($this->attributes['email']);
		} else {
			return asset($this->getAvatarSrcAttribute());
		}
	}


	public function getMyselfAttribute()
	{
		return $this->attributes['id'] === \Sentry::getUser()->id;
	}


	public function notify($message, $mail = false)
	{

	}


	public function getAvatarSrcAttribute()
	{
		return $this->getPath('avatar') . $this->attributes['avatar'];
	}


	public function setAvatarAttribute(UploadedFile $src = null)
	{
		if ($src instanceOf UploadedFile) {
			$dest     = $this->getPath('avatar');
			$filename = sprintf('user-%s-%s.%s', $this->attributes['id'], time(), $src->getClientOriginalExtension());
			$src->move($dest, $filename);
			$this->removeAvatar();
			$this->attributes['avatar'] = $filename;

			$image = Image::make($this->getAvatarSrcAttribute())
			              ->resize(300, null, function ($constraint) {
				              $constraint->aspectRatio();
			              })
			              ->save();
		}

		return;
	}


	// Remove the avatar photo tf the user, set it to null.
	// BIG NOTE: the save() must be called manually to update the value.
	public function removeAvatar()
	{
		if ( ! empty( $this->attributes['avatar'] )) {
			$image = $this->getAvatarSrcAttribute();
			if (File::exists($image)) {
				File::delete($image);
			}
			$this->attributes['avatar'] = null;
		}

		return;
	}


	public function hasPhoto()
	{
		return ! empty( $this->attributes['avatar'] );
	}


	public function getAccessLogUrlAttribute()
	{
		return route('admin.user.show.access', $this->attributes['id']);
	}


	public function getHistoryLogUrlAttribute()
	{
		return route('admin.user.show.history', $this->attributes['id']);
	}


	public function getProfileUrlAtribute()
	{
		return route('admin.user.show', $this->attributes['id']);
	}


	public function getPrivilegeUrlAttribute()
	{
		return route('admin.user.privilege', $this->attributes['id']);
	}


	/**
	 * Copied utiiity code from basemodel since we cannot extends from the basemodel
	 */
	public function scopeApplyFilter($query, $filters = [])
	{
		if ( ! empty( $filters )) {
			if ( ! empty( $filters['name'] )) {
				$query->where(function ($query) use ($filters) {
					return $query->whereLike('first_name', $filters['name'])
					             ->orWhereLike('last_name', $filters['name']);
				});
			}
			if ( ! empty( $filters['email'] )) {
				$query->whereLike('email', $filters['email']);
			}
			if ( ! empty( $filters['group'] )) {
				$groups_id = $filters['group'];
				$query->whereHas('groups', function ($query) use ($groups_id) {
					return $query->whereIn('id', $groups_id);
				});
			}
		}

		return $query;
	}


	protected function label($type = null, $content = null, $attributes = [])
	{
		if (empty( $type )) {
			$type = 'secondary circular';
		}
		if (is_array($attributes)) {
			$attributes = Html::attributes($attributes);
		}
		$pattern = '<span class="label %s" %s>%s</span>';

		return sprintf($pattern, $type, $attributes, $content);
	}


	public function labelError($attributes = null)
	{
		$icon = Html::icon('fa-trash fa-lg error');

		return $this->label('alert circular', $icon, $attributes);
	}


	public function labelSuccess($attributes = null)
	{
		$icon = Html::icon('fa-check fa-lg');

		return $this->label('success circular', $icon, $attributes);
	}


	protected function getLikePattern($wrapping = 'both')
	{
		switch (strtolower($wrapping)) {
			case 'left':
				$pattern = "%%%s";
				break;
			case 'right':
				$pattern = "%s%%";
				break;
			case 'both':
				$pattern = "%%%s%%";
				break;
			default:
				throw new InvalidArgumentException('The wrapping value for LIKE condition must be either `left`, `right` or `both`');
		}

		return $pattern;
	}


	public function scopeWhereLike($query, $field, $value, $wrapping = 'both')
	{
		$pattern = self::getLikePattern($wrapping);

		return $query->where($field, 'LIKE', sprintf($pattern, $value));
	}


	public function scopeOrWhereLike($query, $field, $value, $wrapping = 'both')
	{
		$pattern = self::getLikePattern($wrapping);

		return $query->orWhere($field, 'LIKE', sprintf($pattern, $value));
	}


	protected function tooltip($text)
	{
		$tooltip = [
			'title'        => $text,
			'data-tooltip' => '',
			'class'        => 'has-tip tip-top',
		];

		return $tooltip;
	}


	public function getStatusLabelAttribute()
	{

		$throttle = Sentry::findThrottlerByUserId($this->id);
		if ($throttle->isBanned()) {
			$tooltip = $this->tooltip('user banned on ' . $throttle->banned_at);
			$icon    = Html::icon('fa fa-times fa-lg');

			return $this->label('alert circular', $icon, $tooltip);
		}
		if ($throttle->isSuspended()) {
			$tooltip = $this->tooltip('user suspended at ' . $throttle->banned_at);
			$icon    = Html::icon('fa fa-times fa-lg');

			return $this->label('secondary circular', $icon, $tooltip);
		}
		if ($this->isActivated()) {
			$tooltip = $this->tooltip('activated');
			$icon    = Html::icon('fa fa-check fa-lg');

			return $this->label('success circular', $icon, $tooltip);
		} else {
			$tooltip = $this->tooltip('not activated');
			$icon    = Html::icon('fa fa-clock-o fa-lg');

			return $this->label('secondary circular', $icon, $tooltip);
		}
	}

}
