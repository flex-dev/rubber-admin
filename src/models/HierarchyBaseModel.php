<?php namespace Rubber\Admin;

/**
 * A base model containing common methods and property.
 */
use Baum\Node;
use Input, Config, Validator;

abstract class HierarchyBaseModel extends Node implements RubberModelInterface
{

	protected $guarded = [];

	public static $rules = [];

	public static $messages = [];

	protected $path = [];

	protected $validator = null;

	protected static $errors = [];

	protected static $sortOptions = [
		'sort'      => 'id',
		'direction' => 'desc'
	];


	/**
	 * Listen for save event for calling validation before saving,
	 */
	protected static function boot()
	{
		parent::boot();

		// static::saving(function($model)
		// {
		// 	echo 'saving';
		// 	return $model->validate();
		// });
	}


	public function validate($input = [])
	{
		$rules      = static::$rules;
		$messages   = static::$messages;
		$attributes = empty( $input )
			? static::getAttributes()
			: $input;

		$validation = Validator::make($attributes, $rules);
		if ($validation->passes()) {
			return true;
		}
		static::$errors = $validation->messages();

		return false;
	}


	public function getErrors()
	{
		return static::$errors;
	}


	public function hasErrors()
	{
		return ! empty( static::$errors );
	}


	// Using sort and direction specify in the query string to setup sorting condition on the model
	public function scopeApplySort($query, $defaultSort = null, $defaultDirection = 'asc')
	{
		$direction = Input::has('direction')
			? Input::get('direction')
			: $defaultDirection;
		if (( $direction != 'asc' ) && ( $direction != 'desc' )) {
			$direction = Config::get('rubber/admin::styles.paginator.defaultSort');
		}
		$sort = Input::has('sort')
			? Input::get('sort')
			: $defaultSort;
		if (empty( $sort )) {
			$sort = 'id';
		}
		static::$sortOptions['sort']      = $sort;
		static::$sortOptions['direciton'] = $direction;

		return $query->orderBy($sort, $direction);
	}


	// autometically add filter for seeing trashed / non trashed data
	public function scopeApplyFilter($query, $filter = null)
	{
		switch ($filter) {
			case 'trashed':
				return $query->onlyTrashed();
				break;
			case 'all':
				return $query->withTrashed();
				break;
			default:
				break;
		}

		return $query;
	}


	// Return the current model sorting, the applySort() must be called to get a good value
	public function getSort()
	{
		return $this->sortOptions;
	}


	public function getPath($folder)
	{
		if (empty( $this->path[$folder] )) {
			throw new Exception('The path ' . $folder . ' is not defined in the ' . get_class($this));
		}

		return $this->path[$folder];
	}


	public function getStatusTextAttribute()
	{
		if ( ! isset( $this->attributes['status'] )) {
			return 'n/a';
		}

		return $this->attributes['status']
			? 'Enabled'
			: 'Disabled';
	}


	public function getMissingImageAttribute()
	{
		return 'images/missing.png';
	}

	// reload the model data from the database, or just use the original value if the data has been deleted.
	// Taken from Baum\Baum model
	public function reload()
	{
		if ( ! $this->exists) {
			$this->syncOriginal();
		} else {
			$fresh = static::find($this->getKey());

			$this->setRawAttributes($fresh->getAttributes(), true);
		}

		return $this;
	}

}
