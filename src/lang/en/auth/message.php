<?php

return array(
    'account_not_found' => 'The specified account does not exists.',
    'account_not_activated' =>'The specified account is not activated.',
    'account_suspended' => 'The account has been tempolary suspend.',
    'account_banned' => 'The account has been banned,'
);