<?php namespace Rubber\Admin\Exception;

	class PathInvalidException extends \Exception{
		protected $message = 'A path variable is undefined or empty.';

		public function __construct($message = null){
			if(!empty($message)){
				$this->message = $message;
			}
		}
	}

?>