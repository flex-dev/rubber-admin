<?php namespace Rubber\Admin\Exception;

	class PathNotFoundException extends \Exception{
		protected $message = 'The specify path is not defined in the class';

		public function __construct($message = null){
			if(!empty($message)){
				$this->message = $message;
			}
		}
	}

?>