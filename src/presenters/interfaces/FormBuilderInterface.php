<?php namespace Rubber;
interface FormBuilderInterface{

	public function checkbox($type, $name, $value = null, $checked = null, $options = array());

	public function file($name, $options = array());
}