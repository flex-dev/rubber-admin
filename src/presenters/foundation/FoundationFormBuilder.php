<?php namespace Rubber;

use Illuminate\Routing\UrlGenerator, Illuminate\Html\FormBuilder, Illuminate\Html\HtmlBuilder, Illuminate\Session\Store as Session, Form;
use View;

class FoundationFormBuilder extends \Illuminate\Html\FormBuilder
{

	/**
	 * Create a new form builder instance, need this for accepting necessary injection from the service provider
	 *
	 * @param  \Illuminate\Html\HtmlBuilder     $html
	 * @param  \Illuminate\Routing\UrlGenerator $url
	 * @param  string                           $csrfToken
	 *
	 */

	public function __construct(HtmlBuilder $html, UrlGenerator $url, $csrfToken)
	{
		$this->url       = $url;
		$this->html      = $html;
		$this->csrfToken = $csrfToken;
	}


	public function checkbox($name, $value = null, $checked = null, $options = [])
	{
		return View::make('rubber::common.forms.switch')
		            ->with(compact('type', 'name', 'value', 'checked', 'options'));
	}


	/**
	 * Create a custom file input field
	 *
	 * @param string $name
	 * @param array  $options
	 *
	 * @return string
	 */
	public function file($name, $options = [])
	{
		if (empty( $options['id'] )) {
			$options['id'] = $name;
		}
		if ( ! empty( $option['class'] )) {
			$options['class'] .= 'fileInput';
		} else {
			$options['class'] = 'fileInput';
		}

		return View::make('rubber::common.forms.fileInput')
		            ->with(compact('name', 'options'));
	}
}