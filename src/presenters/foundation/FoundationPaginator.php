<?php namespace Rubber\Admin;

use config;
use Illuminate\Pagination\Presenter;

class FoundationPaginator extends Presenter
{

    public function getActivePageWrapper($text)
    {
        $button = Config::get('rubber/admin::view.pagination.button.active');

        return sprintf('<li class="current"><a class="%s">%s</a></li>', $button, $text);
    }


    public function getDisabledTextWrapper($text)
    {
        $button  = Config::get('rubber/admin::view.pagination.button.class');
        $disable = Config::get('rubber/admin::view.pagination.button.disable');

        return sprintf('<li><a class="%s %s">%s</a></li>', $button, $disable, $text);
    }


    public function getPageLinkWrapper($url, $page, $rel = null)
    {
        $button = Config::get('rubber/admin::view.pagination.button.class');

        return sprintf('<li><a href="%s" class="%s">%s</a></li>', $url, $button, $page);
    }
}

?>