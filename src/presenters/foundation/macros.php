<?php
	Html::macro('charset', function( $charset = null)
	{
		if (empty($charset)) {
			$charset = strtolower(\Config::get('app.encoding'));
		}
		$tag = '<meta http-equiv="Content-Type" content="text/html; charset=%s" />';
		return sprintf($tag, (!empty($charset) ? $charset : 'utf-8'));
	});

	Html::macro('meta', function($name, $value = null)
	{
		if(empty($name)){
			return false;
		}
		$tag = '<meta name="%s" content="% s" />';
		return sprintf($tag, $name, is_null($value) ? '' : $value);
	});

	Html::macro('errorMessages', function(Illuminate\Support\MessageBag  $errors){
		return View::make('rubber::common/error_msg')
			->with('message', $errors);
	});

	// Common function for createing a button
	Html::macro('button', function($type='button', $label='button', $attributes=array()){
		$attributes['class'] = empty($attributes['class']) ? '' : $attributes['class'];
		switch($type){
			case 'anchor':
				$tag = '<a %s>%s</a>';
				break;
			case 'button' : // Fall through
			case 'submit' : // Fall through
			case 'reset' : // Fall through
			default:
				$attributes['class'] = Config::get('rubber/admin::styles.button').$attributes['class'];
				$tag = '<button type="'.$type.'" %s>%s</button>';
				break;
		}
		if(!empty($attributes) && is_array($attributes)){
			foreach($attributes as $k => &$attr){
				$attr = $k.'="'.$attr.'"';
			}
			$attributes = implode(' ', $attributes);

			// TODO : something like $attributes = Utililty::array_flatten(', ', $attributes);
		}
		else{
			$attributes = '';
		}
		return sprintf($tag, $attributes, $label);
	});

	Html::macro('show', function($text, $url, $attributes=array()){
		$attributes['href'] = $url;
		$attributes['class'] = Config::get('rubber/admin::styles.show_button_class', '').' '.array_get($attributes, 'class', '');
		$pattern = '%s <span class="show-for-medium-up">%s</span>';
		$text = sprintf($pattern, Html::icon(array_get($attributes, 'icon', Config::get('rubber/admin::styles.show_button_icon', ''))), $text);
		return Html::button('anchor', $text, $attributes);
	});

	/**
	 * Create a button for "Adding data"
	 * @param String $text - text to be shown on the button
	 * @param String $url - url to be use on the button
	 * @param Array $attributes - other attribute to be apply on the button
	 * @return  String
	 * @todo add something like Config::get('common.class.add') for using as a button class instead of hardcoding
	 * @todo add default url if the url is not supply ?
	 */
	Html::macro('create', function($text, $url, $attributes=array()){
		$attributes['href'] = $url;
		$attributes['class'] = Config::get('rubber/admin::styles.create_button_class', '').' '.array_get($attributes, 'class', '');
		$pattern = Config::get('rubber/admin::styles.button_pattern', '%s %s');
		$text = sprintf($pattern, Html::icon(array_get($attributes, 'icon', Config::get('rubber/admin::styles.create_button_icon', ''))), $text);
		return Html::button('anchor', $text, $attributes);
	});


	/**
	 * Create a button for "Editting data"
	 * Usage: {{ Html::edit( 'text', URL::action('Controller@edit', $id ), $attributes) }}
	 * @param String $text - text to be shown on the button
	 * @param String $url - url to be use on the button
	 * @param Array $attributes - other attribute to be apply on the button
	 * @return  String
	 * @todo add something like Config::get('common.class.edit') for using as a button class instead of hardcoding
	 * @todo add default url if the url is not supply ?
	 *
	 */
	Html::macro('edit', function($text, $url, $attributes=array()){
		$attributes['href'] = $url;
		$attributes['class'] = Config::get('rubber/admin::styles.edit_button_class', '').' '.array_get($attributes, 'class', '');
		$pattern = Config::get('rubber/admin::styles.button_pattern', '%s %s');
		$text = sprintf($pattern, Html::icon(array_get($attributes, 'icon', Config::get('rubber/admin::styles.edit_button_icon', ''))), $text);
		return Html::button('anchor', $text, $attributes);
	});

	/**
	 * Create a button for "Deleting data", actually generate a form with submit buttom and "delete" method
	 * @param String $text - text to be shown on the button
	 * @param String $url - url to be use on the button
	 * @param Array $attributes - other attribute to be apply on the button
	 * @return  String
	 * @todo add something like Config::get('common.class.delete') for using as a button class instead of hardcoding
	 * @todo add default url if the url is not supply ?
	 * @todo do something with the href attribute, since the button is actually a form, maybe need "action" instead ?
	 */
	Html::macro('delete', function($text, $url, $attributes=array()){
		if(empty($attributes['class'])){
			$attributes['class'] = ' ';
		}
		$attributes['delete_form_class'] = Config::get('rubber/admin::styles.delete_form_class', 'form-delete');
		$attributes['delete_button_class'] = Config::get('rubber/admin::styles.delete_button_class', 'disable button small').' '.$attributes['class'] ;
		$attributes['delete_button_icon_class'] = isset($attributes['icon']) ? $attributes['icon'] : Config::get('rubber/admin::styles.delete_button_icon_class', 'fa-trash-o');
		return View::make('rubber::common.macros.delete_form')
			->with(compact('text', 'url', 'attributes'));
	});

	Html::macro('restore', function($text, $url, $attributes=array()){
		$attributes['href'] = $url;
		$attributes['class'] = Config::get('rubber/admin::styles.restore_button_class', '').' '.array_get($attributes, 'class', '');
		$pattern = Config::get('rubber/admin::styles.button_pattern', '%s %s');
		$text = sprintf($pattern, Html::icon(array_get($attributes, 'icon', Config::get('rubber/admin::styles.create_button_icon', ''))), $text);
		return Html::button('anchor', $text, $attributes);
	});

	HTML::macro('sort', function($field, $label = null, $direction=null, $url = null){
		if(empty($field)){
			$field = 'id';
		}
		$icon = Config::get('rubber/admin::styles.paginator.icon');
		$direction = Config::get('rubber/admin::styles.paginator.direction');
		$tip = $field.', ascending';

		// Building icon based on current sorting field and direction
		if(Input::has('sort')){
			if(Input::get('sort') === $field){
				$currentDirection = Input::get('direction', 'asc');
				switch($currentDirection){
					case 'asc':
						$icon = Config::get('rubber/admin::styles.paginator.iconAsc');
						$direction = 'desc';
						$tip = $field.' decending';
						break;
					case 'desc':
						$icon = Config::get('rubber/admin::styles.paginator.iconDesc');
						$direction = 'asc';
						$tip = $field.', ascending';
						break;
					default:
						$icon = Config::get('rubber/admin::styles.paginator.icon');
						$direction = 'asc';
						$tip = $field.', ascending';
						break;
				}
			}
		}
		$label = Html::icon($icon) . $label;
		$url = empty($url) ? Request::url() : $url;
		$query = Request::query();

		$query['sort'] = $field;
		$query['direction'] = $direction;

		$queryString = http_build_query($query);
		$tag = '<a href="%s?%s" title="click to sort by %s" class="has-tip tip-top" data-tooltip>%s</a>';
		return sprintf($tag, $url, $queryString, $tip, $label);
	});

	Html::macro('reset', function($text='reset', $attributes=array()){
		$attributes['class'] = empty($attributes['class']) ? '' : $attributes['class'];
		$attributes['class'] .= ' '.Config::get('rubber/admin::styles.reset_button_class');
		if(Config::has('rubber/admin::styles.reset_button_icon_class')){
			$text = Html::icon(Config::get('rubber/admin::styles.reset_button_icon_class')).' '.$text;
		}
		return Html::button('reset', $text, $attributes);
	});

	Html::macro('submit', function($text='submit', $attributes=array()){
		$attributes['class'] = empty($attributes['class']) ? '' : $attributes['class'];
		$attributes['class'] .= ' '.Config::get('rubber/admin::styles.submit_button_class');
		if(isset($attributes['icon'])){
			$text = Html::icon($attributes['icon']).' '.$text;
		}
		elseif(Config::has('rubber/admin::styles.submit_button_icon_class')){
			$text = Html::icon(Config::get('rubber/admin::styles.submit_button_icon_class')).' '.$text;
		}
		return Html::button('submit', $text, $attributes);
	});

	Html::macro('cancel', function($text='return', $attributes=array()){
		$attributes['href'] = URL::previous();

		$attributes['class'] = Config::get('rubber/admin::styles.cancel_button_class', '').' '.array_get($attributes, 'class', '');
		$pattern = Config::get('rubber/admin::styles.button_pattern', '%s %s');
		$text = sprintf($pattern, Html::icon(array_get($attributes, 'icon', Config::get('rubber/admin::styles.cancel_button_icon_class', ''))), $text);

		// $attributes['class'] = empty($attributes['class']) ? '' : $attributes['class'];
		// $attributes['class'] .= ' '.Config::get('rubber/admin::styles.cancel_button_class');

		// if(Config::has('rubber/admin::styles.cancel_button_icon_class')){
		// 	$text = Html::icon(Config::get('rubber/admin::styles.cancel_button_icon_class')).' '.$text;
		// }
		return Html::button('anchor', $text, $attributes);
	});

	Html::macro('icon', function($icon, $baseIcon = null){
		if(empty($baseIcon)){
			$tag = '<i class="fa %s"></i>';
			$icon = sprintf($tag, $icon);
		}
		else{
			$tag = '<span class="fa-stack"><i class="fa fa-stack-1x %s"></i><i class="fa fa-stack-xx %s"></i></span>';
			$icon = sprintf($tag, $icon, $stackIcon);
		}
		return $icon;
	});


	/**
	 * Appends the last modified time to the file name to make sure that whenever the file is updated, it gets refreshed and never gets cached when there are changes.
	 * Also determine the path to asset, switch between workbench and public folder
	 *
	 * @param  string  $path
	 * @param  boolean $cache_bust
	 *
	 * @return string
	 * @todo find a better way to switch the path since this would be necessaly only for development
	 */
	Html::macro('asset', function( $path , $cacheBust = true, $params=array())
	{
		$full_path = public_path() . '/'.$path;
		if ( !File::exists($full_path) ) {
			//throw new Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException( $full_path );
			return Html::alert('The file : '.$path.' not found.', 'alert');
		}

		$buster = '';

		// Add the last modified time for the file as a parameter.
		//if ( $cache_bust ) {
			$buster =  '?m=' . filemtime( $full_path );
		//}

		// Return the correct element based on the file extension.
		switch ( File::extension($path) ) {
			case 'css':
				return Html::style( $path . $buster );
			case 'js':
				return Html::script( $path . $buster );
			case 'jpg':
			case 'png':
			case 'gif' :
				return Html::image($path.$buster);
			default:
				return URL::asset($path . $buster);
		}
	});

	Html::macro('flash', function($path , $cacheBust = true, $params=array()){
		$full_path = public_path() . '/'.$path;
		if( !File::exists($full_path) ) {
			throw new Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException( $full_path );
		}
		$buster =  '?m=' . filemtime( $full_path );
		return View::make('rubber::common.macros.flashObj')->with('file', $path.$buster)->with('params', $params);
	});

	Html::macro('help', function($text){
		return View::make('rubber::common.macros.help')
			->with(compact('text'));
	});

	Html::macro('alert', function($text, $title=null, $class=null){
		return View::make('rubber::common.macros.alertbox')
			->with(compact('text', 'title', 'class'));
	});

	Html::macro('label', function($type=null, $content=''){
		return sprintf('<span class="%s label">%s</span>', $type, $content);
	});

	Html::macro('trashed', function($content='trashed'){
		return sprintf('<span class="alert round label">%s</span>', $content);
	});


	Html::macro('filter', function($name, $filters, $options = array()){
		$id = Str::quickRandom(5);
		$options = $options + array(
			'size' => null,
			'hasContent' => null,
			'button' => null
		);
		extract($options);
		return View::make('rubber::common.macros.filter')
			->with(compact('name', 'filters', 'id', 'size', 'hasContent', 'button'));
	});


	Form::macro('error', function($errors, $field){
		if($errors->has($field)){
			$error = $errors->first($field);
			return '<small>'.$error.'</small>';
		}
	});

	Form::macro('search', function($text = 'Search'){
		return '<button type="submit" class="button tiny primary"><i class="fa fa-filter fa-lg"></i> '.$text.'</button>';
	});
	Form::macro('resetSearch', function($text = 'Reset'){
		return '<a href="'.Request::url().'" class="tiny secondary button"><i class="fa fa-lg fa-times"></i> '.$text.'</a>';
	});


