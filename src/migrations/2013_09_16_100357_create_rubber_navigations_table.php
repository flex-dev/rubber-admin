<?php

use Illuminate\Database\Migrations\Migration;

class CreateRubberNavigationsTable extends Migration {

	public function up()
	{
		Schema::create('rubber_navigations', function($table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('description', 255)->nullable();
			$table->string('route_name', 255)->nullable();;
			$table->string('route_to', 255)->nullable();;
			$table->string('icon')->nullable();;
			$table->integer('parent_id')->index()->nullable();;
			$table->integer('lft')->index()->nullable();;
			$table->integer('rght')->index()->nullable();;
			$table->integer('depth')->index()->nullable();;
			$table->timestamps();
			$table->softDeletes();

		});
	}

	public function down()
	{
		Schema::drop('rubber_navigations');
	}
}