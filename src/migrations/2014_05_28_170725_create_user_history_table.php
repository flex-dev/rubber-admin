<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_history', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->index()->nullable();
			$table->integer('access_id')->index()->nullable();
			$table->text('content');
			$table->binary('data')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_history');
	}

}
