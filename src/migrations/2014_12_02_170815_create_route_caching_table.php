<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRouteCachingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rubber_bibles', function(Blueprint $table) {
			$table->increments('id');
			$table->text('name')->nullable();
			$table->text('description')->nullable();
			$table->string('route', 255)->index();
			$table->text('data')->nullable();
			$table->enum('method', array('get', 'post', 'patch', 'delete', 'put', 'head'))->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rubber_bibles');
	}

}
