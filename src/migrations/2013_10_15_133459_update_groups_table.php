<?php

use Illuminate\Database\Migrations\Migration;

class UpdateGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('groups', function($table)
		{
			$table->string('description')->nullable()->after('permissions');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		 $table->dropColumn('description');
	}

}