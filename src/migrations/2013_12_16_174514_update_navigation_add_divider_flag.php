<?php

use Illuminate\Database\Migrations\Migration;

class UpdateNavigationAddDividerFlag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rubber_navigations', function($table)
		{
		   	$table->boolean('is_divider')->nullable()->after('depth');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		  Schema::table('rubber_navigations', function($table)
		{
		   	$table->dropColumn('is_divider');
		});
	}

}