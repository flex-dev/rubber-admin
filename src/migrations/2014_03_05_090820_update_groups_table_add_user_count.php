<?php

use Illuminate\Database\Migrations\Migration;

class UpdateGroupsTableAddUserCount extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('groups', function($table)
		{
			$table->integer('total_users')->nullable()->default(0)->after('permissions');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('groups', function($table)
		{
			$table->dropColumn('total_users');
		});
	}

}