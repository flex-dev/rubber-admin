<?php

use Illuminate\Database\Migrations\Migration;

class CreateLoginHistoryTable extends Migration {
	public function up()
	{
		Schema::create('user_access_logs', function($table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id')->nullable();
			$table->string('ip', 30)->nullable();
			$table->string('user_agent', 255)->nullable();
		});
	}

	public function down()
	{
		Schema::drop('login_history');
	}
}