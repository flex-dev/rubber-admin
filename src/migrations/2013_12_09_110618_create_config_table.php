<?php

use Illuminate\Database\Migrations\Migration;

class CreateConfigTable extends Migration {

	public function up()
	{
		Schema::create('rubber_configurations', function($table) {
			$table->increments('id');
			$table->string('name', 255);
			$table->string('value', 255)->nullable();
			$table->string('description', 255)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('rubber_configurations');
	}

}