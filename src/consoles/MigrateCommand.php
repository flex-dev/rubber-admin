<?php namespace Rubber\Admin\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MigrateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rubber/admin:migrate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Run the migration command';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Installing migration.');
		/**
		 * Here we might need option for user to specify environment options ?
		 */
		$this->call('migrate', array('--package' => 'cartalyst/sentry'));
	 	$this->call('migrate', array('--package' => 'rubber/admin'));
	}
}
