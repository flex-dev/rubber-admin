<?php namespace Rubber\Admin\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AssetsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rubber/admin:assets';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Run the migration command';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		/**
		 * Here we might need option for user to specify environment options ?
		 */
		$this->call('asset:publish', array('package' => 'rubber/admin'));
	}

}
