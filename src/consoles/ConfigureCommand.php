<?php namespace Rubber\Admin\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ConfigureCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rubber/admin:config';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Run the config publish command';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info('Publishing packages configuration files');

		$this->info('Publishing Cartalyst/Sentry configuration files');
		$this->call('config:publish', array('package' => 'cartalyst/sentry'));

		$this->info('Publishing Rubber/Admin configuration files');
		$this->call('config:publish', array('package' => 'rubber/admin'));

		$this->info('Publishing intervention/image configuration files');
		$this->call('config:publish', array('package' => 'intervention/image'));

		$this->info('Publishing thomaswelton/laravel-gravatar configuration files');
		$this->call('config:publish', array('package' => 'thomaswelton/laravel-gravatar'));
	}

}
