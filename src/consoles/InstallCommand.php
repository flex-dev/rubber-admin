<?php namespace Rubber\Admin\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InstallCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rubber/admin:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Run the installation command of rubber/admin package, including migraion, config publishing and asset publishing';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->call('rubber/admin:config');
		$this->call('rubber/admin:assets');
		if ($this->confirm('Have you configured your database yet?')) {
			$this->call('rubber/admin:migrate');
		} else {
			$this->comment('Your database has not been migrated, run artisan rubber/admin:migrate before use');
		}
	}

}
