<?php
return [
	'buttons'                   => 'small secondary-darker button',
	// Here is what would be inside the button. Two %s is required. The first one will be replaced with icon, the second one is the text
	'button_pattern'            => '%s <span class="show-for-medium-up">%s</span>',
	'create_button_class'       => 'small button primary-dark  radius',
	'create_button_icon'        => 'fa-plus-square fa-lg',
	'edit_button_class'         => 'small  primary-dark button radius',
	'edit_button_icon'          => 'fa-pencil fa-lg',
	'delete_form_class'         => 'delete-form',
	'delete_button_class'       => 'small warning button radius',
	'delete_button_icon_class'  => 'fa-trash-o fa-lg',
	'restore_form_class'        => 'restore-form',
	'restore_button_class'      => 'small success button radius',
	'restore_button_icon_class' => 'fa-undo  fa-lg',
	'show_button_class'         => 'small button radius',
	'show_button_icon'          => 'fa-info  fa-lg',
	'submit_button_class'       => 'small success button radius',
	'submit_button_icon_class'  => 'fa-save  fa-lg',
	'reset_button_class'        => 'small secondary button radius',
	'reset_button_icon_class'   => 'fa-revert  fa-lg',
	'cancel_button_class'       => 'small secondary button radius',
	'cancel_button_icon_class'  => 'fa-return  fa-lg',
	/** Here is the setting for paginator. **/
	'paginator'                 => [
		'presenter'        => '	foundation',
		'defaultDirection' => 'asc',
		'icon'             => 'fa-sort pull-left',
		'iconAsc'          => 'fa-sort-amount-asc pull-left',
		'iconDesc'         => 'fa-sort-amount-desc pull-left'
	]
];
