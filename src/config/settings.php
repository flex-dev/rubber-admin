<?php
	return array(
		'application_name' => 'Flex CMS',
		'system_email' => 'admin@email.com',
		'title' => 'Flex CMS ',
		'footer' => array(
			'text' => '&copy; Copyright %s to whom it may concerned'
		),
		/** This is the name of css framework of choice, choose either foundation or bootstrap. foundation is the fallback. **/
		'cssFramework' => 'foundation',

		'routes' => array(
			/** The prefix you would like to use in the admin area **/
			'prefix' => 'admin',
		)
	);
