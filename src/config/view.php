<?php
	return array(
		'pagination' => array(
			'view' => 'rubber::common.pagination.links',
			'button' => array(
				'class' => 'button tiny secondary',
				'active' => 'button tiny',
				'disable' => 'unavailable secondary'
			)
		),
		'navigation' => null,
		// The two here is the path to the file, which you may use for adding more scripts/styles to the base layout,
		'prepends' => array(
			'head' => null, // load this file in the <head>
			'body' => null // and load this file before the end of <body>
		)
	);