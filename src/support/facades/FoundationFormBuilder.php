<?php namespace Rubber\Admin\Support\Facades;
	use Illuminate\Support\Facades\Facade;
	class FoundationFormBuilder extends Facade {
		/**
		 * Get the registered name of the component.
		 *
		 * @return string
		 */
		protected static function getFacadeAccessor() {
			return 'rubber.form';
		 }
	}
