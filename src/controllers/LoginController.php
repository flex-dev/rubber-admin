<?php namespace Rubber\Admin;

use Cartalyst\Sentry\Users\PasswordRequiredException;
use Exception;
use View;
use Validator;
use Input;
use Sentry;
use Lang;
use Redirect;
use Request;
use Event;
use Illuminate\Support\MessageBag as MessageBag;

class LoginController extends \BaseController
{

	public function index()
	{
		return View::make('rubber::login');
	}


	/**
	 * @return mixed
     */
	public function authenticate()
	{
		try {
			$user = Sentry::authenticate([
				'email'    => Input::get('email'),
				'password' => Input::get('password')
			]);

			if ($user) {
				Sentry::login($user);
				Event::fire('rubber/admin::auth.login', [$user]);

				return Redirect::intended(route('admin.home'));
			}
		} catch (PasswordRequiredException $e) {
			$messageBag = new MessageBag;
			$messageBag->add('password', $e->getMessage());
		} catch (Exception $e) {
			$messageBag = new MessageBag;
			$messageBag->add('email', $e->getMessage());
			
		}

		return Redirect::route('admin.login')
		               ->withErrors($messageBag);
	}
}
