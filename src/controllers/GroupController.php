<?php namespace Rubber\Admin;

use View;
use Validation;
use Input;
use Redirect;
use Sentry;
use Illuminate\Support\MessageBag;
use Route;
use Str;
use Exception;
use Rubber\Admin\Group;
use Rubber\Admin\User;
use Rubber\Admin\Bible;
use BaseController;

class GroupController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $groups = Sentry::getGroupProvider()
                        ->createModel()
                        ->with('users')
                        ->paginate(10);

        return View::make('rubber::groups.index')
                   ->with('groups', $groups);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $routes = Bible::getRoutes();

        return View::make('rubber::groups.create')
                   ->with(compact('routes'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $message   = new MessageBag;
        $data      = [
            'name'        => Input::get('name'),
            'description' => Input::get('description')
        ];
        $superuser = Input::get('superuser', false);
        if ($superuser) {
            $data['permissions']['superuser'] = 1;
        }
        try {
            $group = Sentry::createGroup($data);
        } catch (Exception $e) {
            $message->add('error', $e->getMessage());

            return Redirect::route('admin.group.list')
                           ->withInput()
                           ->withErrors($message);
        }

        return Redirect::route('admin.group.permissions', $group->id)
                       ->with('message',
                           'A new user group has been created. You may grant access to the group, using the form below.');
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show(Group $group)
    {
        $members       = $group->users()
                               ->get();
        $definedRoutes = Route::getRoutes();
        $users         = Sentry::getUserProvider()
                               ->createModel()
                               ->get()
                               ->lists('full_name_and_email', 'id');

        return View::make('rubber::groups.show')
                   ->with(compact('group', 'members', 'users', 'definedRoutes'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit(Group $group)
    {
        View::share('group', $group);

        return $this->index();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Group $group)
    {
        $message   = new MessageBag;
        $superuser = Input::get('superuser');

        // Allow process if there  will be atleast 1 superuser group
        if ($group->superuser() && ! $superuser) {
            $groups      = Group::all();
            $supergroups = $groups->filter(function ($item) {
                return $item->superuser();
            });
            if ($supergroups->count() <= 1) {
                $message->add('error', 'There must be at least a group with Super User permission');

                return Redirect::route('admin.group.edit', $group->id)
                               ->withInput()
                               ->withErrors($message);
            }
        }

        // Set superuser flag separately
        $permissions              = $group->getPermissions();
        $permissions['superuser'] = $superuser
            ? 1
            : 0;

        // update group data
        $group->name        = Input::get('name');
        $group->description = Input::get('description');
        $group->permissions = $permissions;
        try {
            $group->save();
        } catch (Exception $e) {
            $message->add('error', $e->getMessage());

            return Redirect::route('admin.group.edit', $group->id)
                           ->withInput()
                           ->withErrors($message);
        }

        return Redirect::route('admin.group.list')
                       ->with('message', 'The user group [' . e($group->name) . '] has been updated');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy(Group $group)
    {
        if ($group->superuser()) {
            return Redirect::route('admin.group.list');
        }
        $group->users()
              ->sync([]);
        $group->delete();

        return Redirect::route('admin.group.list')
                       ->with('message', 'The group ' . $group->name . ' has been deleted');
    }


    // Set group permissions
    public function permissions(Group $group)
    {
        $routes = Bible::getRoutes();
        krsort($routes);
        $bibles = Bible::all();

        return View::make('rubber::groups.permissions')
                   ->with(compact('routes', 'group', 'bibles'));
    }


    // Update permission of a group
    public function grantAccess(Group $group)
    {
        // Note:: Permission set to 0  will be removed form the actual saved permission. The final saved resuly will be only allowed permissions
        $permissions        = Input::get('permissions');
        $group->permissions = $permissions;
        $group->save();

        return Redirect::route('admin.group.permissions', $group->id)
                       ->with('message',
                           'The access permissions of the user group [' . $group->name . '] has been update.');
    }


    // Add a user to a group
    public function addUser(Group $group)
    {
        try {
            $user = Sentry::findUserByID(Input::get('user_id'));
            if ($user->inGroup($group)) {
                $message = new MessageBag;
                $message->add('errors',
                    'The selected user [' . e($user->full_name_and_email) . '] is already a member of the group.');

                return Redirect::route('admin.group.show', $group->id)
                               ->withErrors($message);
            } else {
                $user->addGroup($group);
                $group->total_users = $group->users()
                                            ->count();
                $group->save();

                return Redirect::route('admin.group.show', $group->id)
                               ->with('message',
                                   'The user [' . e($user->full_name_and_email) . '] has been added to the group.');
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            $message = new MessageBag;
            $message->add('errors', 'The selected user does not exist on the database.');

            return Redirect::route('admin.group.show', $group->id)
                           ->withErrors($message);
        }
    }


    // remove a user from a group
    public function removeUser(Group $group, User $user)
    {
        $user->removeGroup($group);

        return Redirect::route('admin.group.show', $group->id)
                       ->with('message',
                           'The user [' . e($user->full_name_and_email) . '] has been removed from the group.');
    }

}
