<?php namespace Rubber\Admin;

use Cartalyst\Sentry\Groups\GroupNotFoundException;
use Cartalyst\Sentry\Users\UserAlreadyActivatedException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Input;
use Request;
use Sentry;
use Redirect;
use View;
use Validator;
use Illuminate\Support\MessageBag;
use Mail;
use Queue;
use Config;
use Exception;
use Session;
use Route;
use Rubber\Admin\Group;
use Rubber\Admin\User;

class UserController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		Input::flash();
		$users      = User::with('groups')
		                  ->applyFilter(Input::all())
		                  ->paginate(10);
		$groupsList = Group::lists('name', 'id');

		return View::make('rubber::users.index')
		           ->with(compact('users', 'groupsList'));
	}


	public function logout()
	{
		Sentry::logout();
		Session::forget('rubber');

		return Redirect::route('admin.login');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$groups = Group::lists('name', 'id');

		
		return View::make('rubber::users.create')
		           ->with(compact('groups'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules      = User::$rules;
		$messages   = User::$messages;
		$validation = \Validator::make(Input::all(), $rules, $messages);
		if ($validation->passes()) {
			try {
				if (Input::get('activate')) {
					
					$user = Sentry::createUser([
						'email'      => Input::get('email'),
						'password'   => Input::get('password'),
						'first_name' => Input::get('first_name'),
						'last_name'  => Input::get('last_name'),
						'activated'  => 1
					]);
				} else {
					$user = Sentry::register([
						'email'      => Input::get('email'),
						'password'   => Input::get('password'),
						'first_name' => Input::get('first_name'),
						'last_name'  => Input::get('last_name'),
					]);
				}

			} catch (\Exception $e) {
				$message = new \MessageBag;
				$message->add('error', $e->getMessage());

				return Redirect::route('admin.user.create')
				               ->withErrors($message)
				               ->withInput();
			}
			$groups = Input::get('group_ids', []);
			foreach ($groups as $group) {
				try {
					$group = Sentry::findGroupById($group);
					$user->addGroup($group);
					$group->total_users = $group->users()
					                            ->count();
					$group->save();
				} catch (Exception $e) {
					// $message->add('errors', $e->getMessage());
					continue;
				}
			}
			if ( ! $user->isActivated()) {
				Queue::push('Rubber\Admin\UserController@deliverAccountActivationMail', ['id' => $user->id]);
			}

			return Redirect::route('admin.user.show', $user->id)
			               ->with('message',
				               'A new user has been created. You may add the user to user groups using the form below.');
		}

		return Redirect::route('admin.user.create')
		               ->withErrors($validation)
		               ->withInput();
	}


	public function roles(User $user)
	{

		try {
			$groups = Sentry::getGroupProvider()
			                ->createModel()
			                ->orderBy('name', 'desc')
			                ->get();

			return View::make('rubber::users.assignRoles')
			           ->with(compact('user', 'groups'));
		} catch (GroupNotFoundException $e) {
			$messageBag = new  MessageBag();
			$messageBag->add('errors',
				'There is no user group available. Please created at least one group before processing.');

			return Redirect::route('admin.group.list')
			               ->withErrors($messageBag);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param \Rubber\Admin\User $user
	 *
	 * @return Response
	 *
	 */
	public function show(User $user)
	{
		$access  = $user->access()
		                ->orderBy('id', 'desc')
		                ->limit(10)
		                ->get();
		$history = $user->history()
		                ->with('access')
		                ->with('user')
		                ->orderBy('id', 'desc')
		                ->limit(10)
		                ->get();
		$routes  = Route::getRoutes();
		$bibles  = Bible::all();

		return View::make('rubber::users.show')
		           ->with(compact('user', 'access', 'history', 'bibles'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \Rubber\Admin\User $user
	 *
	 * @return Response
	 *
	 *
	 */
	public function edit(User $user)
	{
		return View::make('rubber::users.edit')
		           ->with(compact('user'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Rubber\Admin\User $user
	 *
	 * @return Response
	 *
	 */
	public function update(User $user)
	{
		// Override the rules for editting,
		$rules          = User::$rules;
		$rules['email'] = 'required|email|unique:users,email,' . $user->id;
		$messages       = User::$messages;

		$validation = Validator::make(Input::all(), $rules, $messages);
		if ($validation->passes()) {
			$user->fill(Input::except('password'));
			// NOTE: Quick fix to force avatar to be set.
			// Should be find since the laravel 4.1.x is deprecated and should have no further update
			// 4.2 might not need this
			$user->avatar = Input::file('avatar');
			if (Input::get('password')) {
				$user->password = Input::get('password');
			}
			$user->save();

			return Redirect::route('admin.user.show', $user->id)
			               ->with('message', 'The user account of ' . $user->full_name_or_email . ' has been updated.');
		} else {
			return Redirect::route('admin.user.edit', $user->id)
			               ->withErrors($validation)
			               ->withInput();
		}
	}


	public function updatePassword(User $user)
	{
		try {
			$credentials = [
				'email'    => Input::get('email'),
				'password' => Input::get('current_password')
			];

			$user              = Sentry::authenticate($credentials, false);
			$rules['password'] = User::$rules['password'];
			$messages          = User::$messages;
			$validation        = Validator::make(Input::all(), $rules, $messages);
			if ($validation->passes()) {
				$user->password = Input::get('password');
				$user->save();

				return Redirect::route('admin.user.show', $user->id)
				               ->with('message',
					               'The password of account of ' . $user->full_name_or_email . ' has been updated.');
			} else {
				return Redirect::route('admin.user.show', $user->id)
				               ->withErrors($validation)
				               ->withInput();
			}
		} catch (\Exception $e) {
			$message = new \MessageBag;
			$message->add('email', $e->getMessage());

			return Redirect::to(route('admin.user.edit', $user->id) . '#password')
			               ->withErrors($message);
		}

	}


	/**
	 * Find all user inside a group.
	 *
	 * @param  Group $group instance of Cartalyst Group as defined in package onfic file
	 *                      $group should be feeded from Route::bind('group_name') or something similar to get the
	 *                      group.
	 */
	public function group(Group $group)
	{
		// BUG: cannot use the commented line below directly. There is a bug in Sentry code not using the User and Group Eloquent model as defined in the package config file in some implementation. Have to fetch them directly from Sentry instead.
		// Issue: https://github.com/cartalyst/sentry/pull/263
		// $users = $group->users->paginate(10);

		//  Sentry::findAllUsersInGroup return whole  collection of uses, cannot paginate.
		//  Sentry::findAllUsersInGroup use findAll, then filter by inGroup method for each user, results in slow processing when having bigger number of users
		// $users = Sentry::findAllUsersInGroup($group);
		$users = Sentry::getUserProvider()
		               ->createModel()
		               ->join('users_groups', 'users_groups.user_id', '=', 'users.id')
		               ->where('users_groups.group_id', $group->id)
		               ->with('groups')
		               ->paginate(10);

		return View::make('rubber::users.index')
		           ->with(compact('group', 'users'));
	}


	public function removePhoto(User $user)
	{
		if ( ! empty( $user->avatar )) {
			$user->removeAvatar();
			$user->avatar = null;
			$user->save();
			$message = sprintf('The photo of %s has been removed', $user->full_name_or_email);

			return Redirect::route('admin.user.show', $user->id)
			               ->with('message', $message);
		} else {
			$message = sprintf('The user %s does not have a photo. ', $user->full_name_or_email);

			return Redirect::route('admin.user.show', $user->id)
			               ->with('message', $message);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy(User $user)
	{
		if ($user->isSuperUser()) {
			return Redirect::route('admin.user.list')
			               ->with('message', 'warning:A super user cannot be deleted.');
		}
		$user->delete();
		$message = sprintf('The user %s has been deleted', $user->full_name_or_email);

		return Redirect::route('admin.user.list')
		               ->with('message', $message);
	}


	public function restore(User $user)
	{
		if ($user->trashed()) {
			$user->restore();
			$message = sprintf('The user %s has been restored', $user->full_name_or_email);

			return Redirect::route('admin.user.list')
			               ->with('message', $message);
		}

		return Redirect::route('admin.user.list');
	}


	public function ban(User $user)
	{
		try {
			$throttle = Sentry::findThrottlerByUserId($user->id);
		} catch (\Exception $e) {
			$message = new \MessageBag;
			$message->add('error', $e->getMessage());

			return Redirect::route('admin.user.show', $user->id)
			               ->withErrors($message);
		}
		$throttle->ban();
		$message = sprintf("The user %s has been put in the ban list", $user->full_name_and_email);

		return Redirect::route('admin.user.show', $user->id)
		               ->with('message', $message);
	}


	public function unban(User $user)
	{
		try {
			$throttle = Sentry::findThrottlerByUserId($user->id);
		} catch (\Exception $e) {
			$message = new \MessageBag;
			$message->add('error', $e->getMessage());

			return Redirect::route('admin.user.show', $user->id)
			               ->withErrors($message);
		}
		$throttle->unban();
		$message = sprintf("The user %s has been removed from the ban list", $user->full_name_and_email);

		return Redirect::route('admin.user.show', $user->id)
		               ->with('message', $message);
	}


	public function accessLog(User $user)
	{
		$access = $user->access()
		               ->orderBy('id', 'desc')
		               ->paginate(10);

		return View::make('rubber::users.access')
		           ->with(compact('user', 'access'));
	}


	public function historyLog(User $user)
	{
		$history = $user->history()
		                ->orderBy('id', 'desc')
		                ->paginate(10);

		return View::make('rubber::users.history')
		           ->with(compact('user', 'history'));
	}


	public function lostPassword()
	{
		return View::make('rubber::lost-password');
	}


	public function sendResetMail()
	{
		$message = new MessageBag;
		try {
			$user = Sentry::findUserByLogin(Input::get('email'));
		} catch (Exception $e) {
			$message->add('error', $e->getMessage());

			return Redirect::route('admin.user.lost-password')
			               ->withErrors($message);
		}
		Queue::push('Rubber\Admin\UserController@deliverResetPasswordMail', ['id' => $user->id]);
		$message->add('message', 'A reset password mail will be sent to the specify address');

		return Redirect::route('admin.user.reset-password')
		               ->with('message', $message);
	}


	public function deliverResetPasswordMail($job, $data)
	{
		try {
			$user = Sentry::findUserById($data['id']);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
		$data['token'] = $user->getResetPasswordCode();
		Mail::send('rubber::emails.auth.reminder', $data, function ($message) use ($user) {
			$message->subject('[' . Config::get('rubber/admin::settings.title') . '] Reset Password Reminder');
			$message->from(Config::get('rubber/admin::settings.system_email'));
			$message->to($user->email);
		});
		$job->delete();

		return;
	}


	public function resetPassword($token = null)
	{
		$token = Input::get('token', $token);
		if ($token) {
			$message = new MessageBag;
			try {
				$user = Sentry::findUserByResetPasswordCode($token);
			} catch (UserNotFoundException $e) {
				$message->add('error', $e->getMessage());

				return Redirect::route('admin.user.lost-password', $token)
				               ->withErrors($message);
			}

			if (Input::has('password')) {
				$password = Input::get('password');
				if ($user->checkResetPasswordCode($token)) {
					if ($user->attemptResetPassword($token, Input::get('password'))) {
						return Redirect::route('admin.login')
						               ->with('message', 'Your password has been changed');
					} else {
						$message->add('error',
							'An error has occured while trying to reset your password. Please try again');

						return Redirect::route('admin.user.reset-password', $token)
						               ->withErrors($message);
					}
				} else {
					$message->add('error', 'The reset password token is invalid');

					return Redirect::route('admin.user.lost-password', $token)
					               ->withErrors($message);
				}
			}
		}

		return View::make('rubber::new-password')
		           ->with('token', $token);
	}


	public function privilege(User $user)
	{
		$groups = Sentry::findAllGroups();

		return View::make('rubber::users.privilege')
		           ->with(compact('user', 'groups'));
	}


	public function revokeAccess(User $user)
	{
		$message       = new MessageBag;
		$groups        = Input::get('group_ids', []);
		$currentGroups = $user->groups->lists('id');
		$additions     = array_diff($groups, $currentGroups);
		$deletes       = array_diff($currentGroups, $groups);

		// Add user to new groups
		foreach ($additions as $group) {
			try {
				$group = Sentry::findGroupById($group);
			} catch (Exception $e) {
				$message->add('errors', $e->getMessage());
				continue;
			}
			if ($user->inGroup($group)) {
				$message->add('errors',
					'The selected user [' . e($user->full_name_and_email) . '] is already a member of the group ' . $group->name);
			} else {
				$user->addGroup($group);
				$group->total_users = $group->users()
				                            ->count();
				$group->save();
			}
		}

		// remove user from existing groups
		foreach ($deletes as $group) {
			try {
				$group = Sentry::findGroupById($group);
			} catch (Exception $e) {
				$message->add('errors', $e->getMessage());
				continue;
			}
			if ( ! $user->inGroup($group)) {
				$message->add('errors',
					'The selected user [' . e($user->full_name_and_email) . '] is not a member of the group ' . $group->name);
			} else {
				$user->removeGroup($group);
				$group->total_users = $group->users()
				                            ->count();
				$group->save();
			}
		}

		return Redirect::to($user->privilege_url)
		               ->with('message',
			               'The permissions of [' . e($user->full_name_and_email) . '] has been revoked.');
	}


	public function deliverAccountActivationMail($job, $data)
	{
		try {
			$user = Sentry::findUserById($data['id']);
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
		$data['token'] = $user->getActivationCode();
		Mail::send('rubber::emails.auth.activation', $data, function ($message) use ($user) {
			$message->subject('[' . Config::get('rubber/admin::settings.title') . '] Account Activation');
			$message->from(Config::get('rubber/admin::settings.system_email'));
			$message->to($user->email);
		});
		$job->delete();

		return;
	}


	public function activate($token)
	{
		$message = new MessageBag;
		try {
			$user = Sentry::findUserByActivationCode($token);
			if ($user->attemptActivation($token)) {
				return Redirect::route('admin.login', ['activated' => 1]);
			}
		} catch (UserNotFoundException $e) {
			$message->add('error', 'User was not found.');
		} catch (UserAlreadyActivatedException $e) {
			return Redirect::route('admin.login', ['activated' => 1]);
		} catch (Exception $e){
			return Redirect::route('admin.login', ['activated' => 0])->withErrors($e->getMessage);
		}
		return Redirect::route('admin.login')
		               ->withErrors($message);
	}


	public function resendActivationCode()
	{

	}

}
