<?php namespace Rubber\Admin;

use View;
use Input;

class HistoryController extends \Controller
{

    public function index()
    {
        $history = UserHistory::orderBy('id', 'desc');
        $from    = Input::get('from');
        $to      = Input::get('to');
        if ($from) {
            $history->where('created_at', '>=', $from);
        }
        if ($to) {
            $history->where('created_at', '<=', $to);
        }
        $history = $history->with('access', 'user')
                           ->paginate(10);

        return View::make('rubber::history.index')
                   ->with(compact('history', 'from', 'to'));
    }


    public function byDate()
    {

    }
}
