<?php namespace Rubber\Admin;

use BaseController;
use Baum\MoveNotPossibleException;
use View;
use Redirect;
use Input;
use Validator;
use Route;
use MessageBag;
use Request;

class NavigationController extends BaseController
{

	protected $_root;


	public function __construct()
	{
		$this->_root = Navigation::root();
		if (empty( $this->_root )) {
			// Create default ROOT.
			$this->_root = Navigation::create([
				'name'        => 'Top Level',
				'description' => 'This is the root node',
				'route_name'  => '',
				'route_to'    => '/',
				'icon'        => ''
			]);
		}
	}


	public function missingMethod($parameters = [])
	{
		$message = new MessageBag;
		$message->add('errors', 'The navigation does not exists.');

		return Redirect::route('admin.navigation.list')
		               ->withErrors($message);
	}


	protected function getRoot()
	{
		return $this->_root;
	}


	protected function getTree()
	{
		return $this->_root->getDescendantsAndSelf()
		                   ->all();
	}


	/**
	 * Determine if a route has any variables
	 *
	 * @param $route
	 *
	 * @return bool
	 */

	protected function hasVariables($route)
	{
		$uri = $route->getUri();

		return preg_match('/{+(.*?)}/', $uri);
	}


	// Return an array of "GET" routes, without any parameter requirements.
	protected function getRoutes()
	{
		$definedRoutes = Route::getRoutes()
		                      ->getRoutes();
		$routes        = [];
		foreach ($definedRoutes as $route) {
			if ( ! in_array('GET', $route->getMethods())) {
				continue;
			}
			$name = $route->getName();
			if (empty( $name )) {
				continue;
			}
			if ($this->hasVariables($route)) {
				continue;
			}
			$routes[$name] = $name;
		}

		return $routes;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tree = $this->getTree();

		return View::make('rubber::navigations.index')
		           ->with(compact('tree'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if (Request::ajax()) {
			return View::make('rubber::navigations.partials.quickMenu');
		}
		$tree   = $this->getRoot();
		$tree   = $tree->getNestedList('name');
		$routes = $this->getRoutes();

		return View::make('rubber::navigations.create')
		           ->with(compact('tree', 'routes'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules      = Navigation::$rules;
		$message    = Navigation::$messages;
		$validation = Validator::make(Input::all(), $rules, $message);
		if ($validation->passes()) {
			$navigation = Navigation::create(Input::all());
			try {
				$parent = Navigation::findOrFail(Input::get('parent_id'));
				$navigation->makeChildOf($parent);
			} catch (MoveNotPossibleException $e) {
				if (Request::ajax()) {
					return Redirect::route('admin.navigation.create')
					               ->withErrors($validation)
					               ->withInput();
				}
				$message = new \Illuminate\Support\MessageBag;
				$message->add('error', $e->getMessage());

				return Redirect::route('admin.navigation.create')
				               ->withInput()
				               ->with('errors', $message);
			}
			$navigation->makeChildOf($parent);
			if (Request::ajax()) {
				return 'saved';
			}

			return Redirect::route('admin.navigation.list')
			               ->with('message', 'A new navigation menu has been added.');

		} else {
			return Redirect::route('admin.navigation.create')
			               ->withErrors($validation)
			               ->withInput();
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show(Navigation $navigation)
	{
		$descendants = $navigation->getDescendants();

		return View::make('rubber::navigations.show')
		           ->with(compact('navigation', 'descendants'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Navigation $navigation
	 *
	 * @return Response
	 * @internal param int $id
	 *
	 */
	public function edit(Navigation $navigation)
	{
		$tree   = $this->getRoot();
		$tree   = $tree->getNestedList('name');
		$routes = $this->getRoutes();

		return View::make('rubber::navigations.edit')
		           ->with(compact('navigation', 'tree', 'routes'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param Navigation $navigation
	 *
	 * @return Response
	 *
	 */
	public function update(Navigation $navigation)
	{
		$rules   = Navigation::$rules;
		$message = Navigation::$messages;
		if ($navigation->isRoot()) {
			unset( $rules['parent_id'] );
		}
		$validation = Validator::make(Input::all(), $rules, $message);
		if ($validation->passes()) {
			$navigation->fill(Input::all());
			$navigation->save();
			// move the node of the parent id changes, except the root node which cannot be moved
			if ( ! $navigation->isRoot()) {
				try {
					$parent = Navigation::findOrFail(Input::get('parent_id'));
					$navigation->makeChildOf($parent);
				} catch (MoveNotPossibleException $e) {
					$message = new \Illuminate\Support\MessageBag;
					$message->add('error', $e->getMessage());

					return Redirect::route('admin.navigation.edit', $navigation->id)
					               ->with('errors', $message);
				}
			}

			return Redirect::route('admin.navigation.list')
			               ->with('message', 'The menu ' . $navigation->name . ' has been updated.');

		} else {
			return Redirect::route('admin.navigation.edit', $navigation->id)
			               ->withErrors($validation)
			               ->withInput();
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Navigation $navigation
	 *
	 * @return Response redirect
	 */
	public function destroy(Navigation $navigation)
	{
		if (is_null($navigation->parent_id)) {
			return Redirect::route('admin.navigation.list')
			               ->with('message',
				               'The navigation menu <strong>' . $navigation->name . '</strong> is the root menu. The root menu cannot be deleted.');
		}
		$navigation->delete();

		return Redirect::route('admin.navigation.list')
		               ->with('message',
			               'The navigation menu <strong>' . $navigation->name . '</strong> has been removed.');
	}


	public function reorder()
	{
		$navigations = Navigation::all()
		                         ->toHierarchy();

		return View::make('rubber::navigations.sort')
		           ->with(compact('navigations'));
	}


	public function applyOrder()
	{
		return;
	}

}