<?php namespace Rubber\Admin;

use View;
use Input;
use Redirect;

class MediaController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $medias = Media::orderBy('id', 'desc')
                       ->get()
                       ->lists('link');

        return json_encode($medias);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function upload()
    {
        $file      = new Media;
        $file->src = Input::file('file');
        $file->save();

        return $file->toJson();
    }


    /** Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
