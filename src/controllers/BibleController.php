<?php namespace Rubber\Admin;

use View;
use Request;
use Input;
use Redirect;
use Rubber\Admin\Bible;
use Str;

class BibleController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$routes = Bible::applySort('route', 'asc')->get();
		return View::make('rubber::bibles.index')
			->with(compact('routes'));
	}

	// Read the routes register in laravel, and syn in to database
	public function sync()
	{
		$routesCollection =Bible::getRoutes();
		$plain = array();
		foreach($routesCollection as $namespace => $rs){
			foreach($rs as $controller => $routes){
				foreach($routes as $route){
					$plain[$route->methods[0]][] = $route->name;
				}
			}
		}

		foreach($plain as $method => $route){
			$exists = Bible::where('method', $method)
				->lists('route', 'id');
			$new = array_diff($route, $exists);
			foreach($new as $r){
				$newroute = new Bible;
				$newroute->method = $method;
				$newroute->route = $r;
				$newroute->save();
			}
			$delete = array_diff($exists, $route);
			if(!empty($delete)){
				Bible::where('method', $method)
					->whereIn('route', $delete)
					->delete();
			}
		}
		return Redirect::route('admin.bible.list');
	}

	// Update a data
	public function update(Bible $route)
	{
		$route->fill(Input::all());
		$route->save();
		return $route->toJson();
	}

	// Purge all routes, super dangerous
	public function purge()
	{
		Bible::truncate();
		return Redirect::route('admin.bible.list');
	}

}
