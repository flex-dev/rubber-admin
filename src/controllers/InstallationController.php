<?php namespace Rubber\Admin;

use BaseController;
use Sentry;
use View;
use Redirect;
use Input;
use Validator;

class InstallationController extends BaseController
{

	/**
	 * Start the setup process, only if there os no user with Superuser permission
	 * @return [type] [description]
	 */
	public function setup()
	{
		if (Sentry::findAllUsersWithAccess(['superuser'])) {
			return Redirect::route('admin.login')
			               ->with('message', 'The system has already been setup.');
		}

		return View::make('rubber::install');
	}


	/**
	 * Create a user to initialize the admin system.
	 * @method POST
	 *
	 */
	public function install()
	{
		if (Sentry::findAllUsersWithAccess(['superuser'])) {
			return Redirect::route('admin.login')
			               ->with('message', 'The system has already been setup.');
		}

		$rules   = [
			'email'    => 'required|unique:users|email',
			'password' => 'required|min:5',
			'group'    => 'required'
		];
		$message = [
			'email'    => [
				'required' => 'Please enter a valid email adress.',
				'email'    => 'Please enter a valid email adress.'
			],
			'password' => [
				'required' => 'Please enter a password for this user',
				'min'      => 'The password must be at least :min characters'
			],
			'group'    => [
				'required' => 'Please enter a name of the group'
			]
		];

		$validator = Validator::make(Input::all(), $rules, $message);
		if ($validator->passes()) {
			$group = Input::get('group');
			try {
				$admingroup = Sentry::findGroupByName($group);
			} catch (\Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
				$admingroup = Sentry::createGroup([
					'name'        => $group,
					'description' => Input::get('description'),
					'permissions' => ['superuser' => 1]
				]);
			}
			$user = Sentry::createUser([
				'email'       => Input::get('email'),
				'password'    => Input::get('password'),
				'permissions' => ['superuser' => 1],
				'activated'   => true
			]);

			$user->addGroup($admingroup);

			return Redirect::route('admin.login')
			               ->with('message',
				               'Installation complete. You may now login to the system with the newly created super user account');
		} else {
			return Redirect::route('admin.setup')
			               ->withInput()
			               ->withErrors($validator);
		}
	}
}