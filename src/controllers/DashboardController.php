<?php namespace Rubber\Admin;

use View;

class DashboardController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = \Sentry::getUser();

        return View::make('rubber::dashboard')
                   ->with(compact('user'));
    }
}