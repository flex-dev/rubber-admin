<?php

	View::composer('rubber::common.header.navigation', function($view){
		$root =  Rubber\Admin\Navigation::root();
		$navigation = [];
		if($root){
			$navigations = $root->getDescendants();
			if($navigations){
				$tree = $navigations->toHierarchy();
				if($tree){
					$navigation = $tree;
				}
			}
		}
		$view->with('navigations', $navigation);
	});

	View::composer('rubber::common.messages', function($view){
		if(Session::has('messages')){
			$messages = Session::get('messages');
		}
		else{
			$messages = new Illuminate\Support\MessageBag;
		}
		if(Session::has('message')){
			$messages->add('info', Session::get('message'));
		}
		if(!$messages instanceof Illuminate\Support\MessageBag){
			$messages = new Illuminate\Support\MessageBag($messages);
		}
		$view->with('messages', $messages);
	});


	// autometically load routes data wuth the granted permissions view  is loaded
	View::composer('rubber::users.partials.permissions', function($view){
		$view->with('routes', Route::getRoutes());
	});

	View::composer('rubber::users.partials.info', function($view){
		// $user = Sentry::getUser();
		// $throttle = Sentry::findThrottlerByUserId($user->id);
		// $view->with('throttle', $throttle);
	});

	View::composer('rubber::users.notifications.partials.form', function($view){
		$user = Sentry::getUser();
		$view->with('sender', $user);
	});

	View::composer('rubber::users.index', function($view){
		$groups =Rubber\Admin\Group::orderBy('name')
			->get()
			->lists('url_user_by_group', 'name');
		$view->with(compact('groups'));
	});

	View::composer('rubber::navigations.partials.quickMenu', function($view){
		$tree = Rubber\Admin\Navigation::root();
		$tree = $tree->getNestedList('name');
		$view->with('tree', $tree);
	});

