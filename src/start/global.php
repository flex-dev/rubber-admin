<?php
	include __DIR__."/../routes/rubber.php";
	include __DIR__."/../start/views.php";

	Route::post('queue/receive', function()
	{
		return Queue::marshal();
	});

	switch(Config::get('rubber/admin::settings.presenter', 'foundation')){
		case 'bootstrap':
			include  __DIR__. '/../presenters/bootstrap/macros.php';
			break;
		case 'foundation':

		default:
			include  __DIR__. '/../presenters/foundation/macros.php';
			break;
	}

	try{
		Queue::getIron()->ssl_verifypeer = false;
	}
	catch(Exception $e){

	}
