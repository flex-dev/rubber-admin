<?php
/* This shall be the routing rules for the common admin functionality. Extra rules shall be placed in a separated route file, or the app/routes.php */
$prefix = Config::get('rubber/admin::settings.routes.prefix', 'admin');
Route::group(
	array( 'prefix' => $prefix ),
	function(){

		Route::get('install', array(
			'as' => 'admin.setup',
			'uses' => 'Rubber\Admin\InstallationController@setup'
		));

		Route::post('install', array(
			'as' => 'admin.install',
			'uses' => 'Rubber\Admin\InstallationController@install'
		));

		Route::get('login', array(
			'as' => 'admin.login',
			'uses' => 'Rubber\Admin\LoginController@index'
		));
		Route::get('logout', array(
			'as' => 'admin.logout',
			'uses' => 'Rubber\Admin\UserController@logout'
		));

		Route::post('login', array(
			'as' => 'admin.login.authenticate',
			'uses' => 'Rubber\Admin\LoginController@authenticate'
		));

		Route::get('user/lost-password',  array(
			'as' => 'admin.user.lost-password',
			'uses' => 'Rubber\Admin\UserController@lostPassword'
		));
		Route::post('user/lost-password',  array(
			'as' => 'admin.user.lost-password',
			'uses' => 'Rubber\Admin\UserController@sendResetMail'
		));
		Route::get('user/reset-password/{token?}',  array(
			'as' => 'admin.user.reset-password',
			'uses' => 'Rubber\Admin\UserController@resetPassword'
		));
		Route::post('user/reset-password/{token?}',  array(
			'as' => 'admin.user.reset-password',
			'uses' => 'Rubber\Admin\UserController@resetPassword'
		));


		Route::get('log/history', array(
			'as' => 'admin.history.list',
			'uses' => 'Rubber\Admin\HistoryController@index'
		));

		Route::get('log/history/{history}.json', array(
			'as' => 'admin.history.download',
			'do' => function(Rubber\Admin\UserHistory $history){
				return Response::make($history->data, 200)
					->header('Content-Type', 'application/json')
					->header('Content-Disposition', 'inline;filename='.$history->file_name);
			}
		));
		Route::model('history', 'Rubber\Admin\UserHistory');


		Route::get('user/activate/{activation_code}', array(
			'as' => 'admin.user.activate',
			'uses' => 'Rubber\Admin\UserController@activate'
		));
});

Route::group(
	array(
		'before' => 'adminAuth',
		'prefix' => $prefix
	 ),
	function(){
		Route::get('/', array(
			'as' => 'admin.home',
			'uses' => 'Rubber\Admin\DashboardController@index'
		));

	}
);

// Check the admin session and permission here.
Route::filter('adminAuth', function(){

	if(!Sentry::check()){
		return Redirect::guest(route('admin.login'))
			->with('message', 'Please login to your account before accessing the restricted area.');
	}
	else{
		$route = Route::currentRouteName();

		// admin.home is globally available, as a dashboard for anyone with account, no locking required in whatever case.
		if($route != 'admin.home'){
			$user = Sentry::getUser();
			if(!$user->hasAccess(Route::currentRouteName())){
				return Redirect::route('admin.home')
					->with('message', 'You don\'t have a permission to access the area');
			}
		}
		else{
			// The route has no name, cannot do anything else.
			// MAYBE, we can do some additional special condition checking not related to the permission table here,.
		}

		// Set the pagination view to the one specified inside the view config of the CMS
		// The view.pagination is not autometically overriding the default setting in app\config\view.php. Have to do it manually
		// The code placed here, instead of the AdminServiceProvider,  to apply the setting only in the admin area, not anywhere else.

		Config::set('view.pagination', Config::get('rubber/admin::view.pagination.view'));
	}
});