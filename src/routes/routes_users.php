<?php

	/**
	 * CRUD for users admin
	 */
	Route::get('user', array(
		'as' => 'admin.user.list',
		'uses' => 'UserController@index'
	));

	Route::get('user/group/{group_name}', array(
		'as' => 'admin.user.bygroup',
		'uses' => 'UserController@group'
	));

	Route::get('user/create', array(
		'as' => 'admin.user.create',
		'uses' => 'UserController@create'
	));
	Route::post('user/create', array(
		'as' => 'admin.user.store',
		'uses' => 'UserController@store'
	));

	// special case for updating the current user, without specifying id on the url
	Route::get('user/edit', array(
		'as' => 'admin.self.edit',
		'uses' => 'UserController@edit'
	))->before('injectSelf');

	Route::patch('user/edit', array(
		'as' => 'admin.self.update',
		'uses' => 'UserController@update'
	))->before('injectSelf');

	Route::get('user/{user_id}', array(
		'as' => 'admin.user.show',
		'uses' => 'UserController@show'
	));
	Route::get('user/{user_id}/access', array(
		'as' => 'admin.user.show.access',
		'uses' => 'UserController@accesslog'
	));
	Route::get('user/{user_id}/history', array(
		'as' => 'admin.user.show.history',
		'uses' => 'UserController@historylog'
	));
	Route::get('user/{user_id}/privilege', array(
		'as' => 'admin.user.privilege',
		'uses' => 'UserController@privilege'
	));
	Route::patch('user/{user_id}/privilege', array(
		'as' => 'admin.user.privilege',
		'uses' => 'UserController@revokeAccess'
	));


	Route::get('user/{user_id}/edit', array(
		'as' => 'admin.user.edit',
		'uses' => 'UserController@edit'
	));
	// Route::put('user/{user_id}', array(
	// 	'as' => 'admin.user.update',
	// 	'uses' => 'UserController@update'
	// ));
	Route::patch('user/{user_id}', array(
		'as' => 'admin.user.update',
		'uses' => 'UserController@update'
	));
	Route::delete('user/{user_id}', array(
		'as' => 'admin.user.destroy',
		'uses' => 'UserController@destroy'
	));
	Route::delete('user/{user_id}/ban', array(
		'as' => 'admin.user.ban',
		'uses' => 'UserController@ban'
	));
	Route::patch('user/{user_id}/ban', array(
		'as' => 'admin.user.unban',
		'uses' => 'UserController@unban'
	));

	Route::get('user/{user_id}/password', array(
		'as' => 'admin.user.password',
		'uses' => 'UserController@editPassword'
	));

	Route::patch('user/{user_id}/password', array(
		'as' => 'admin.user.password.update',
		'uses' => 'UserController@updatePassword'
	));

	Route::get('user/{user_id}/password/reset', array(
		'as' => 'admin.user.password.reset',
		'uses' => 'UserController@resetPassword'
	));

	Route::get('user/{user_id}/activation/resend', array(
		 'as' => 'admin.user.resendActivationCode',
		 'uses' => 'UserController@resendActivationCode'
	));

	Route::get('user/{user_id}/edit', array(
		'as' => 'admin.user.edit',
		'uses' => 'UserController@edit'
	));

	Route::get('user/{user_id}/restore', array(
		'as' => 'admin.user.restore',
		'uses' => 'UserController@restore'
	));

	Route::delete('user/{user_id}/photo', array(
		'as' => 'admin.user.photo.delete',
		'uses' => 'UserController@removePhoto'
	));

	// Inject the current user into the controller
	Route::filter('injectSelf', function($route, $request){
		$user = Sentry::getUser();
		$route->setParameter('user', $user);
	});


	//  TODO: handle error when user does not exists, or exception occured
	Route::bind('user_id', function($id){
		try{
			$user = Sentry::findUserById($id);
			return $user;
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{

		}
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
			return;
		}
	});
