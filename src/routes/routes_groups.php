<?php
	Route::get('group', array(
		'as' => 'admin.group.list',
		'uses' => 'GroupController@index'
	));

	Route::get('group/create', array(
		'as' => 'admin.group.create',
		'uses' => 'GroupController@create'
	));
	Route::post('group/create', array(
		'as' => 'admin.group.store',
		'uses' => 'GroupController@store'
	));

	Route::get('group/{group_id}', array(
		'as' => 'admin.group.show',
		'uses' => 'GroupController@show'
	));

	Route::get('group/{group_id}/edit', array(
		'as' => 'admin.group.edit',
		'uses' => 'GroupController@edit'
	));
	Route::put('group/{group_id}/', array(
		'as' => 'admin.group.update',
		'uses' => 'GroupController@update'
	));
	Route::delete('group/{group_id}/', array(
		'as' => 'admin.group.delete',
		'uses' => 'GroupController@destroy'
	));
	Route::patch('group/{group_id}/', array(
		'uses' => 'GroupController@update'
	));
	Route::get('group/{group_id}/permissions', array(
		'as' => 'admin.group.permissions',
		'uses' => 'GroupController@permissions'
	));
	Route::post('group/{group_id}/permissions', array(
		'as' => 'admin.group.permission.grant',
		'uses' => 'GroupController@grantAccess'
	));
	Route::post('group/{group_id}/user', array(
		'as' => 'admin.group.user.add',
		'uses' => 'GroupController@addUser'
	));
	Route::delete('group/{group_id}/user/{user_id}', array(
		'as' => 'admin.group.user.remove',
		'uses' => 'GroupController@removeUser'
	));

	Route::bind('group_name', function($group_name){
		$group = Sentry::findGroupByName($group_name);
		return $group;
	});

	Route::bind('group_id', function($group_id){
		try{
			$group = Sentry::findGroupById($group_id);
			return $group;
		}
		catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
		{
			$message = new  \Illuminate\Support\MessageBags();
			$message->add('errors', $e->getMessage());
			return Redirect::route('admin.group.list')
				->with('message', $message)
				->send();
			die();
		}

	});
