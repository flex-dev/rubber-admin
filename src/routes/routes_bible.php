<?php
	Route::get('bible', array(
		'as' => 'admin.bible.list',
		'uses' => 'BibleController@index'
	));

	Route::post('bible/sync', array(
		'as' => 'admin.bible.sync',
		'uses' => 'BibleController@sync'
	));

	Route::patch('bible/{bible}', array(
		'as' => 'admin.bible.update',
		'uses' => 'BibleController@update'
	));

	Route::delete('bible', array(
		'as' => 'admin.bible.purge',
		'uses' => 'BibleController@purge'
	));

	Route::bind('bible', function($id){
		return Rubber\Admin\Bible::findOrFail($id);
	});
