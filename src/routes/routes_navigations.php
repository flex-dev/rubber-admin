<?php
	/**
	 *  CRUD for Navigations
	 */
	Route::get('navigation', array(
		'as' => 'admin.navigation.list',
		'uses' => 'NavigationController@index'
	));
	Route::get('navigation/create', array(
		'as' => 'admin.navigation.create',
		'uses' => 'NavigationController@create'
	));

	Route::post('navigation', array(
		'as' => 'admin.navigation.store',
		'uses' => 'NavigationController@store'
	));
	Route::get('navigation/trashed', array(
		'as' => 'admin.navigation.trashed',
		'uses' => 'NavigationController@trashed'
	));

	Route::get('navigation/reorder', array(
		'as' => 'admin.navigation.reorder',
		'uses' => 'NavigationController@reorder'
	));

	Route::post('navigation/reorder', array(
		'as' => 'admin.navigation.reorder.apply',
		'uses' => 'NavigationController@applyOrder'
	));
	Route::get('navigation/{navigation_idasas}', array(
		'as' => 'admin.navigation.show',
		'uses' => 'NavigationController@show'
	));
	Route::get('navigation/{navigation_idasas}/edit', array(
		'as' => 'admin.navigation.edit',
		'uses' => 'NavigationController@edit'
	));
	Route::put('navigation/{navigation_idasas}/edit', array(
		'as' => 'admin.navigation.update',
		'uses' => 'NavigationController@update'
	));
	Route::patch('navigation/{navigation_idasas}/edit', array(
		'as' => 'admin.navigation.update',
		'uses' => 'NavigationController@update'
	));
	Route::delete('navigation/{navigation_idasas}', array(
		'as' => 'admin.navigation.delete',
		'uses' => 'NavigationController@destroy'
	));

	Route::bind('navigation_idasas', function($id){
		$navigation = Rubber\Admin\Navigation::withTrashed()->findOrFail($id);
		return $navigation;
	});
