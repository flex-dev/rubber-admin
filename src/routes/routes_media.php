<?php
	Route::get('media', array(
		'as' => 'admin.media.list',
		'uses' => 'MediaController@index'
	));
	Route::post('media/upload', array(
		'as' => 'admin.media.upload',
		'uses' => 'MediaController@upload'
	));
	Route::get('media/delete', array(
		'as' => 'admin.media.delete',
		'uses' => 'MediaController@destroy'
	));
