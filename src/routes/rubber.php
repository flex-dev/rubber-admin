<?php
	include __DIR__."/../routes/base_routes.php";
	$prefix = Config::get('rubber/admin::settings.routes.prefix', 'admin');
	Route::group(
		array(
			'before' => 'adminAuth',
			'prefix' => $prefix,
			'namespace' => 'Rubber\Admin'
		 ),
		function(){
			include __DIR__."/../routes/routes_users.php";
			include __DIR__."/../routes/routes_navigations.php";
			include __DIR__."/../routes/routes_groups.php";
			include __DIR__."/../routes/routes_media.php";
			include __DIR__."/../routes/routes_bible.php";
		}
	);

