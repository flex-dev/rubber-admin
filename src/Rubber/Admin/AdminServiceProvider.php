<?php namespace Rubber\Admin;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('rubber/admin');
		include __DIR__.'/../../start/global.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{


		$this->app['config']->package('rubber/admin',  __DIR__.'/../../config', 'rubber/admin');
		$this->app['rubber.chronicle'] = $this->app->share(function ($app) {
			$history = new \Rubber\Admin\Chronicle(\Sentry::getUser());
			return $history;
		});
		$this->registerCommands();
		$this->setupCssFramework();
	 	// $this->app['events']->subscribe('Rubber\Admin\Event\UserEventHandler');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('rubber.form', 'rubber.chronicle');
	}

	public function registerCommands()
	{
		$this->registerMigrateCommand();
		$this->registerConfigureCommand();
		$this->registerAssetsCommand();
		$this->registerInstallCommand();

		$this->commands(
			'rubber-admin::commands.migrate',
			'rubber-admin::commands.config',
			'rubber-admin::commands.assets',
			'rubber-admin::commands.install'
		);
	}

	public function registerMigrateCommand()
	{
		$this->app['rubber-admin::commands.migrate'] = $this->app->share(function($app)
		{
			return new Console\MigrateCommand;
		});
	}

	public function registerConfigureCommand()
	{
		$this->app['rubber-admin::commands.config'] = $this->app->share(function($app)
		{
			return new Console\ConfigureCommand;
		});
	}

	public function registerAssetsCommand()
	{
		$this->app['rubber-admin::commands.assets'] = $this->app->share(function($app)
		{
			return new Console\AssetsCommand;
		});
	}

	public function registerInstallCommand()
	{
		$this->app['rubber-admin::commands.install'] = $this->app->share(function($app)
		{
			return new Console\InstallCommand;
		});
	}

	public function setupCssFramework(){
		switch($this->app['config']->get('rubber-admin:settings.presenter')){

			case 'bootstrap':
				\View::addNamespace('rubber', __DIR__.'/../../views/bootstrap');
				$this->app['rubber.form'] = $this->app->share(function ($app) {
					$form = new \Rubber\BootstrapFormBuilder(
						$app['html'], $app['url'], $app['session']->getToken()
					);
					return $form->setSessionStore($app['session.store']);
				});
				break;

			case 'foundation':
			default:
				\View::addNamespace('rubber',  __DIR__.'/../../views/foundation');
				$this->app['rubber.form'] = $this->app->share(function ($app) {
					$form = new \Rubber\FoundationFormBuilder(
						$app['html'], $app['url'], $app['session']->getToken()
					);
					return $form->setSessionStore($app['session.store']);
				});
				break;
		}
	}


}