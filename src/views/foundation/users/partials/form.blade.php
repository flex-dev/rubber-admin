
						<div class="row">
							<div class="large-6 columns {{ $errors->has('email') ? 'error' : '' }}">
								<label for="email">
									Email Address {{ Html::help('The system will use this email as a login credential. An activation mail will be sent to this email address.') }}
								</label>
								{{ Form::text('email', null, array( 'placeholder' => 'email@webdomain.com')) }}
								{{ Form::error($errors, 'email') }}
							</div>
							@unless(empty($user))
							<div class="row">
								<div class="large-6  small-12 columns  {{ $errors->has('last_name') ? 'error' : '' }}">
									{{ Form::label('Upload Photo (image file, 300KB maximum)') }}
									{{ Tinker::file('avatar') }}
								</div>
							</div>
							@endunless
						</div>

						<div class="row">
							<div class="large-6 columns {{ $errors->has('first_name') ? 'error' : '' }}">
								{{ Form::label('First Name') }}
								{{ Form::text('first_name') }}
								{{ Form::error($errors, 'first_name') }}
							</div>
							<div class="large-6 columns  {{ $errors->has('last_name') ? 'error' : '' }}">
								{{ Form::label('Last Name') }}
								{{ Form::text('last_name') }}
							</div>
						</div>

						@unless(!empty($user))
							@include('rubber::users.partials.password')
						@endunless