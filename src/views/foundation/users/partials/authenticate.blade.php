
	<div class="panel">
		<p>To verify an ownership of the account. Please enter the account email and current password .</p>
	</div>
	<div class="row">
		<div class="large-6 columns {{ $errors->has('email') ? 'error' : '' }}">
			{{ Form::label('email', 'Account Email') }}
			{{ Form::text('email', null, array( 'placeholder' => 'account@email.com')) }}
			{{ Form::error($errors, 'email') }}
		</div>
		<div class="large-6 columns {{ $errors->has('email') ? 'current_password' : '' }}">
			{{ Form::label('current_password', 'Current Password') }}
			{{ Form::password('current_password')  }}
			{{ Form::error($errors, 'current_password') }}

		</div>
	</div>