			<table class="responsive">
				<thead>
					<tr>
						<th width="150">Method</th>
						<th >Name</th>
						<th>Path</th>
					</tr>
				</thead>
				<tbody>
				@unless($permissions)
				<tr>
					<td colspan="2">No special permission granted to this user</td>
					<td></td>
				</tr>
				@else

				@foreach($permissions as $permission=>$allowed)
					<tr>
						@if($permission == 'superuser')
							<td colspan="3">Super user permission. All actions allowed without restriction</td>
						@else
							@if($routes->hasNamedRoute($permission))
								<?php $route = $routes->getByName($permission); ?>
								<td>
									@foreach($route->getMethods() as $method)
									<span class="label secondary">{{ $method }}</span>
									@endforeach
								</td>
								<td>
									<?php
										$bible = $bibles->first(function($idx, $item) use ($route){
											return in_array(strtoupper($item->method), $route->getMethods()) && ($item->route == $route->getName());
										});
									?>
									@if($bible)
									<strong>{{ $bible->name }}</strong><br>
									@endif
									<small>{{ $permission }}</small>
								</td>
								<td> {{ $route->getPath() }}</td>
							@else
								<td></td>
								<td><s>{{ $permission }}</s></td>
								<td><small>Route not exists</small></td>
							@endif
						@endif
					</tr>
				@endforeach

				@endunless
				</tbody>
			</table>