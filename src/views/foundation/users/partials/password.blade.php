				<div class="row">
					<div class="large-12 columns">
						<div class="row">
							<div class="large-4 small-12 columns  {{ $errors->has('password') ? 'error' : '' }}">
								{{ Form::label('password', 'Password') }}
								{{ Form::password('password', array( 'placeholder' => 'atleast 5 characters')) }}
								{{ Form::error($errors, 'password') }}
							</div>

							<div class="large-4 small-12 columns">
								{{ Form::label('password_confirmation', 'Retype the password') }}
								{{ Form::password('password_confirmation' , array( 'placeholder' => 'must matches with the password')) }}
							</div>

							<div class="large-4 small-12 columns ">
								<label class="strengthText">Strength</label>
								<div class="progress strengthTest round">
									<span class="meter"></span>
								</div>
								{{ Html::help('An estimated time for the password to be cracked by a malicious user.') }}
							</div>

						</div>


					</div>
				</div>

				@section('bottomScript')
				{{ Html::script('packages/rubber/admin/vendors/dropbox/zxcvbn.js')}}
				<script>
					$(function(){
						$("input[name=password]").on('keyup', function(ev){
							var password = $(this).val();
							var strength = zxcvbn(password);
							var info = '';
							var meter = $(this).closest('.row').find('.progress');
							meter.attr('data-score', strength.score);
							var crackTime = String(strength.crack_time_display);
						 	if(strength.score == 4) {
								if (crackTime.indexOf("years") !=-1) {
									meter.attr('data-score', 6);
								}
								else if (crackTime.indexOf("months") !=-1) {
									meter.attr('data-score', 5);
								}
								else if (crackTime.indexOf("centuries") !=-1) {
									meter.attr('data-score', 7);
								}
								meter.addClass('success').removeClass('alert secondary');

							}
							else if(strength.score < 2){
								meter.addClass('secondary').removeClass('alert success');
							}
							else{
								meter.addClass('alert').removeClass('secondary success');
							}
							meter.siblings('.strengthText').text('Password cracked in : '+crackTime);

						});
					});
				</script>
				@stop
