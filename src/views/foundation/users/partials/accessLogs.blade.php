
			<table class="responsive">
				<thead>
					<tr>
						<th width="150">Date</th>
						<th width="100">From IP</th>
						<th>User Agent</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="3"></td>
					</tr>
				</tfoot>
				<tbody>
					@foreach($access as $log)
					<tr>
						<td><span class="has-tip tip-right" data-tooltip title="{{ $log->created_at }}">{{ $log->created_at->diffForHumans() }}</span></td>
						<td>{{ $log->ip }}</td>
						<td><small>{{ $log->user_agent }}</small></td>
					</tr>
					@endforeach
				</tbody>
			</table>
