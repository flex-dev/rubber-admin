		<table class="table table-striped table-hover" data-sort="true">
			<thead>
				<tr>
					<th></th>
					<th data-toggle="true">User</th>
					<th class="text-center"  width="100" >Status</th>
					<th data-hide="phone,tablet"  class="text-center">Last Login</th>
					<th data-hide="phone" width="200"  class="text-center">Action</th>
			</thead>
			<tfoot>
				<tr>
					<td colspan="6">
						<a href="{{ route('admin.user.create') }}" class="button tiny primary"><i class="fa fa-lg fa-plus"></i> New User</a>
					</td>
				</tr>
			</tfoot>
			<tbody>
			@unless($users->count())
				<tr><td colspan="6"  class="text-center">No user found</td>
			@else
			@foreach($users as $user)
				<tr>
					<td width="100" ><img class="th rounded" src="{{ $user->avatar_image_path }}"></td>
					<td >
						<ul class="no-bullet">
							<li>
								{{ Html::icon('fa-user') }} {{{ $user->full_name_or_email }}}<br>
							</li>
							<li>{{ Html::icon('fa-envelope') }} {{{ $user->email }}}</li>
						</ul>
						<ul class="inline-list">
						@foreach($user->groups as $group)
							<li class="label secondary">{{{ $group->name }}}</li>
						@endforeach
						</ul>
					</td>
					<td  class="text-center">{{ $user->status_label }}</td>
					<td class="text-center">
						{{ $user->last_login ? $user->last_login->diffForHumans() : 'never' }}</td>
					<td  class="text-center">
						{{ HTML::show(null, route('admin.user.show', $user->id)) }}
						{{ HTML::edit(null, route('admin.user.edit', $user->id)) }}

						{{ HTML::delete(null, route('admin.user.destroy', $user->id)) }}

					</td>
				</tr>
				@endforeach
			@endunless
			</tbody>

		</table>

		{{ $users->links() }}