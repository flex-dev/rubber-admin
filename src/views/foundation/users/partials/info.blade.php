			<?php
				$throttle = Sentry::findThrottlerByUserId($user->id);
			?>
			<div class="large-3 medium-12 columns">

				<div class="panel radius">
					<div class="row">
						<div class="large-4 small-6 medium-4 columns" style="padding-right: 0">
							<img class="th circular " src="{{ $user->avatar_image_path }}" style="width: 5em; height: 5em; border-radius: 3em;"><br><br>
						</div>
						<div class="large-8 small-6 medium-8 columns">
							<strong>{{{ $user->full_name }}}</strong>
							<br><br>
							@if(!$user->activated)
								<span class="label alert">not activated</span>
							@endif
							@if($suspended = $throttle->isSuspended())
								<span class="label alert">suspended</span>
							@endif
							@if($suspended = $throttle->isBanned())
								<span class="label alert">banned</span>
							@endif
							@if($user->activated)
								<span class="label success">active</span>
							@else
								<span class="label secondary">pending</span>
							@endif
						</div>
					</div>

					<div class="row">
						<div class="large-12 small-6 medium-3 columns">
							{{ Html::edit(' Edit User', route('admin.user.edit', $user->id), array( 'class' => 'expand')) }}
						</div>
						@if(Sentry::getUser()->hasAccess('admin.user.privilege'))
						<div class="large-12 small-6 medium-3 columns end">
							<a href="{{ $user->privilege_url }}" class="button tiny secondary expand">
								<i class="fa fa-lg fa-gavel"></i> Privileges
							</a>
						</div>
						@endif

						@if(!$user->myself)
						<div class="large-12 small-6 medium-3 columns">
							{{ Html::delete(' Delete', route('admin.user.destroy', $user->id), array( 'class' => 'expand' )) }}
						</div>
						<div class="large-12 small-6 medium-3 columns">
							@if($banned =$throttle->isBanned())
							{{ Form::open(array( 'route' => array('admin.user.unban', $user->id), 'method' => 'patch' )) }}
								{{ Html::submit(' Unban', array('icon' => 'fa-legal fa-lg' , 'class' => 'expand')) }}
							{{ Form::close() }}
							@else
							{{ Html::delete(' Ban', route('admin.user.ban', $user->id), array('icon' => 'fa-warning fa-lg' , 'class' => 'expand')) }}
							@endif
						</div>
						@endif

						@if(Sentry::getUser()->hasAccess('admin.user.list'))
							<div class="large-12 columns">
								<a href="{{ route('admin.user.list') }}" class="button tiny secondary expand"><i class="fa fa-lg fa-users"></i> Users List</a>
							</div>
						@endif

					</div>

				</div>
			</div>
