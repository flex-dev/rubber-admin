<div class="panel">
	{{ Form::open(array( 'url' => route('admin.user.list'), 'method' => 'get')) }}
	<div class="row">
		<div class="large-12 columns">
			{{ Form::label('name', 'Name') }}
			{{ Form::text('name') }}
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			{{ Form::label('email', 'Email') }}
			{{ Form::text('email') }}
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			{{ Form::label('group', 'Group') }}
			{{ Form::select('group[]', $groupsList, null, array( 'class' => 'chosen', 'multiple' => true)) }}
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<label for="">&nbsp;</label>
			{{ Form::search() }}
			{{ Form::resetSearch() }}
		</div>
	</div>
	{{ Form::close() }}
</div>