@extends('rubber::layouts.table')

	@section('content_header')
		Manage Roles of: {{ $user->full_name }}
	@stop
	@section('table')
		<table>
			<thead>
				<tr>
					<th>Assign</th>
					<th>Name</th>
					<th>Permissions</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tfoot>
			<tbody>
			@unless($groups)
				<tr>
					<td colspan="3">No groups available</td>
				</tr>
			@else
				@foreach($groups as $group)
				<tr>
					<td></td>
					<td>{{ $group->name }}</td>
					<td>
						@foreach($group->permissions as $k=>$perm)
							{{$k }} : {{ $perm }}<br>
						@endforeach
					</td>
				</tr>
				@endforeach
			@endunless
			</tbody>
		</table>

	@stop