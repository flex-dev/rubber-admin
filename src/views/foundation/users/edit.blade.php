@extends('rubber::layouts.form')

	@section('content_header')
		User: {{{ $user->full_name_or_email}}} - edit
	@stop
	@section('form')
		<div class="row">
			@include('rubber::users.partials.info')
			<div class="large-9 columns">

				<div class="section-container auto" data-section data-options="deep_linking: true">

					<section class="active">
						<p class="title" data-section-title><a href="#information">Account Information</a></p>
						<div class="content" data-section-content data-slug="information">
							<header><h3><i class="fa fa-user fa-lg"></i> Account Information</h3></header>

							<div class="row">
								<div class="large-10 columns">
								@if($user->id == Sentry::getID())
									{{ Form::model($user, array(
										'route' => array('admin.self.update'),
										'method' => 'patch',
										'files' => true
										))
									}}
									@else
									{{ Form::model($user, array(
										'route' => array('admin.user.update', $user->id),
										'method' => 'patch',
										'files' => true
										))
									}}
									@endif

									@include('rubber::users.partials.form')
									@include('rubber::users.partials.password')

									<div class="row">
										<div class="large-12 columns">
											{{ HTML::submit( 'Save') }}
										</div>
									</div>

									{{ Form::close() }}
								</div>

								<div class="large-2 columns">
									@unless(empty($user))
										<label>Current Photo</label>
										<img src="{{ $user->avatar_image_path }}" class="th round">
											@if($user->hasPhoto())
										{{ Html::delete( ' remove photo', route('admin.user.photo.delete', $user->id) )}}
										@endif
									@else

									@endunless
								</div>
							</div>

						</div>
					</section>

<!--
					<section>
						<p class="title" data-section-title><a href="#password">Change Account Password</a></p>
						<div class="content" data-section-content data-slug="password">
							<header><h3><i class="fa fa-key fa-lg"></i> Change Account Password</h3></header>
							{{ Form::open(array(
								'route' => array('admin.user.password.update', $user->id),
								'method' => 'patch'))
							}}

							<div class="row">
								<div class="large-10 columns">
									@include('rubber::users.partials.authenticate')
								</div>
							</div>


							<div class="row">
								<div class="large-12 columns">
									{{ HTML::submit( 'Change Password') }}
								</div>
							</div>

							{{ Form::close() }}
						</div>
					</section>
 -->
				</div>
			</div>
		</div>

	@stop