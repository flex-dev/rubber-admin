@extends('rubber::layouts.base')
	@section('content')
		<header>
			<h2>{{ Html::icon('fa-users') }} User : {{{ $user->full_name_or_email }}}</h2>
		</header>
		<div class="row">

			@include('rubber::users.partials.info')

			<div class="large-9 columns">
				<div class="section-container auto" data-section data-options="deep_linking: true">

					<section>
						<p class="title" data-section-title><a href="#group-permissions">Group Permissions</a></p>
						<div class="content" data-section-content data-slug="group-permissions">
							<header><h3>Group Permissions</h3></header>
							@include('rubber::users.partials.permissions', ['permissions' => $user->getMergedPermissions(), 'bibles' => $bibles])
						</div>
					</section>

				</div>
			</div>
		</div>
	@stop