@extends('rubber::layouts.form')

	@section('content_header')
		User: {{{ $user->full_name_or_email }}} - Privilege
	@stop
	@section('form')
		<div class="row">
			@include('rubber::users.partials.info')
			<div class="large-9 columns">
				{{ Form::open(array('url' => $user->privilege_url, 'method' => 'patch') ) }}
				<table>
				<thead>
					<tr>
						<th width="80"></th>
						<th width="200">Group</th>
						<th>Description</th>
					</tr>
				</thead>
				<tbody>
					@foreach($groups as $group)
					<tr>
						<td class="text-center">
							<label for="group_{{ $group->id }}">{{ Form::checkbox('group_ids[]', $group->id, $user->inGroup($group), array( 'id' => 'group_'.$group->id)) }}</label>
						</td>
						<td><label for="group_{{ $group->id }}">{{{ $group->name }}}</label></td>
						<td>{{{ $group->description }}}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td colspan="2">{{ Html::submit('Grant Access') }}</td>
					</tr>
				</tfoot>
				</table>

				{{ Form::close() }}
			</div>
		</div>

	@stop