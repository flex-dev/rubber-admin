@extends('rubber::layouts.detail')

	@section('content_header')
	<h2>User Information : {{{ $user->full_name_or_email }}}</h2>
	@stop

	@section('detail')

		<div class="row">

			@include('rubber::users.partials.info')

			<div class="large-9 columns">
				<header><h3>Access Log</h3></header>
				@include('rubber::users.partials.accessLogs', compact('access'))
				{{ $access->links() }}
			</div>
		</div>
	@stop