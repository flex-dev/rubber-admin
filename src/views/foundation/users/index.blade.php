@extends('rubber::layouts.base')

	@section('content_header')

	@stop

	@section('content')

		<div class="row">
			<div class="large-9 columns">
				<header>
					<h2>{{ Html::icon('fa-users') }} Users Account
						@if(!empty($group))
						<small>{{{ $group->name }}}</small>
						@endif
					</h2>
				</header>
				@include('rubber::users.partials.table', compact('user'))
			</div>
			<div class="large-3 columns">
				<header>
					<h2><i class="fa fa-search fa-lg"></i> Search Users</h2>
				</header>
				@include('rubber::users.partials.filters')
			</div>
		</div>


	@stop