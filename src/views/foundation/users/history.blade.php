@extends('rubber::layouts.detail')

	@section('content_header')
	<h2>User Information : {{{ $user->full_name_or_email }}}</h2>
	@stop

	@section('detail')

		<div class="row">

			@include('rubber::users.partials.info')

			<div class="large-9 columns panel">
				<header><h3>Activity History</h3></header>
				@include('rubber::users.partials.historyLog', compact('history'))
				{{ $history->links() }}
			</div>
		</div>
	@stop