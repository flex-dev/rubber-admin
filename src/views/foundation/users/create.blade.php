@extends('rubber::layouts.base')

	@section('content')

		<header>
			<h2><i class="fa fa-lg fa-user"></i> Create New User</h2>
		</header>

		{{ Form::open(array( 'route' => 'admin.user.store', 'method' => 'post' )) }}
		<div class="row">
			<div class="large-8 columns">
				<div class="panel">
					<header><h3>Account Information</h3></header>
					@include('rubber::users.partials.form')
					{{ Html::submit(' Create Account
					') }}
					<a href="{{ route('admin.user.list') }}" class="small button secondary"><i class="fa fa-times fa-lg"></i> Cancel</a>
				</div>
			</div>
			<div class="large-4 columns">
				<div class="panel">
					<header><h3>Optional Settting</h3></header>

					<div class="row">
						<div class="large-6 columns">
							<label>
								{{ Tinker::checkbox('activate', 1) }}
								Activate on create
							</label>

						</div>
					</div>

					<div class="row">
						<div class="large-12 columns">
							<label for="">Privileges</label>
							{{ Form::select('group_ids[]', $groups, null, array( 'class' => 'chosen', 'multiple' => true)) }}
						</div>
					</div>

				</div>

			</div>
		</div>
		{{ Form::close() }}

	@stop