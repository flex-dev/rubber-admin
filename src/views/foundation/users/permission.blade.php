		<fieldset>
			<legend>Privileges</legend>
			<div class="row">
				{{ Form::label('Group') }}
				{{ Form::select('group', $groups) }}
			</div>
			<div class="row">
				{{ Form::label('Additional Permission') }}
				{{ Form::text('permissions') }}
			</div>
			{{ Form::submit() }}
		</fieldset>