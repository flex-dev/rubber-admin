@extends('rubber::layouts.table')

	@section('table')
		<div class="row">
			<div class="large-3 columns">
				<header><h2>Filter</h2></header>
				<div class="panel">
					{{ Form::open(array('route' => 'admin.history.list', 'method' => 'GET')) }}
					<div class="row">
						<div class="large-12  small-12 medium-6 columns">
							{{ Form::label('from') }}
							{{ Form::input('date', 'from', $from) }}
						</div>
						<div class="large-12 small-12 medium-6 columns">
							{{ Form::label('from') }}
							{{ Form::input('date', 'to', $to) }}
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							{{ Html::submit( 'Search' )}}
						</div>
					</div>
					{{ Form::close() }}
				</div>
			</div>
			<div class="large-9 columns">
				<header>
					<h2>Activities Logs</h2>
				</header>
				@include('rubber::history.table', compact('history'))
				{{ $history->links() }}
			</div>
		</div>

	@stop