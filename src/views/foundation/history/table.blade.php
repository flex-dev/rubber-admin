
			<table class="responsive">
				<thead>
					<tr>
						<th width="100">User</th>
						<th width="150">Date</th>
						<th>Log</th>
						<th class="text-center" width="100">Session</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="4"></td>
					</tr>
				</tfoot>
				<tbody>
					@foreach($history as $log)
					<tr>
						<td>
							<a href="{{ $log->user->profile_url }}" class="has-tip tip-top" data-tooltip title="{{ $log->user->full_name_or_email }}"><img src="{{ $log->user->avatar_image_path }}"></a>
						</td>
						<td><span class="has-tip tip-top" data-tooltip title="{{ $log->created_at }}">{{ $log->created_at->diffForHumans() }}</span></td>
						<td>
							{{ $log->content }}
						</td>
						<td class="text-center">
							@if($log->dataExists())
							<a href="{{ $log->download_url }}" class="tiny button secondary" data-tooltip title="download data file"><i class="fa fa-download fa-lg"></i></a>
							@endif
							<a href="#" class="button secondary tiny has-tip tip-top" data-tooltip title="{{ $log->access->ip }}<br> {{ $log->access->user_agent }}">
								<i class="fa fa-lg fa-history"></i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
