<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Account activation</h2>

		<p>
			To activate your account, please click on this link :<br>
			 <a href="{{ route('admin.user.activate', $token ) }}">{{ route('admin.user.activate', $token ) }}</a>
		</p>
	</body>
</html>