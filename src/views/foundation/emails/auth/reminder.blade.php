<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>

		<div>
			To reset your password, please click this link and follow the instruction:
			<a href="{{ route('admin.user.reset-password', array($token)) }}">{{ route('admin.user.reset-password', array($token)) }}</a>
		</div>
	</body>
</html>