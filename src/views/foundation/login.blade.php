@extends('rubber::layouts.blank')

	@section('content')
	<div id="loginForm" class="panel">
		<h1>Login</h1>
		@if($errors->any())
			@include('rubber::common.errors', compact('errors'))
		@endif

		@if(Request::query('activated') !== null)
		{{ Html::alert('Your account has been activated', null, 'info')}}
		@endif

		{{ Form::open(array( 'route' => 'admin.login', 'method' => 'post' ))}}
			<div class="row collapse {{ $errors->has('email') ? 'error' : '' }}">
				<div class="small-10 columns">
					{{ Form::email('email', null, array( 'placeholder' => 'Email Address', 'autocomplete' => 'off')) }}
				</div>
				<div class="small-2 columns">
					<span  class="postfix  has-tip tip-top" data-tooltip title="An email address registered as a user account.">{{ Html::icon('fa-key') }}</span>
				</div>
			</div>
			<div class="row collapse {{ $errors->has('password') ? 'error' : '' }}">

				<div class="small-10 columns">
					{{ Form::password('password', array( 'placeholder' => 'Password', 'autocomplete' => 'off')) }}
				</div>
				<div class="small-2 columns">
					<span class="postfix has-tip tip-top" data-tooltip title="The password accompany with the account">{{ Html::icon('fa-asterisk') }}</span>
				</div>
			</div>
			<button class="button tiny primary" type="submit"><i class="fa fa-sign-in fa-lg"></i> Login</button>
			<a href="{{ route('admin.user.lost-password') }}" class="tiny button secondary"><i class="fa fa-question-circle"></i> Lost Password</a>

		{{ Form::close() }}
	</div>
	@stop