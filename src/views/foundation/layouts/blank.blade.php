@include('rubber::common.html_head')
<head>
	@include('rubber::common.html_meta_tags')
	{{ Html::style('packages/rubber/admin/vendors/foundation/css/normalize.css') }}
	{{ Html::style('packages/rubber/admin/vendors/foundation/css/foundation.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/foundation/css/grid-f5.css') }}
	{{ Html::style('packages/rubber/admin/vendors/font-awesome/css/font-awesome.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/minstrap/css/bootstrapdoc.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/chosen/chosen.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/froala/css/froala_editor.min.css') }}
	{{ Html::asset('packages/rubber/admin/css/rubber-admin.css') }}
	{{ Html::script('packages/rubber/admin/vendors/foundation/js/vendor/custom.modernizr.js') }}
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,600,italic' rel='stylesheet' type='text/css'>
	{{ Html::style('packages/rubber/admin/vendors/jquery/ui/jquery-ui.theme.min.css') }}

</head>
<body>

	@yield('content')

	{{ Html::script('packages/rubber/admin/vendors/jquery/jquery.min.js') }}
	{{ Html::script('packages/rubber/admin/vendors/foundation/js/foundation.min.js') }}
	{{ Html::script('packages/rubber/admin/vendors/foundation/js/foundation/foundation.section.js') }}
	{{ Html::script('packages/rubber/admin/vendors/chosen/chosen.jquery.min.js') }}
	{{ Html::script('packages/rubber/admin/js/script.js') }}
	{{ Html::script('packages/rubber/admin/vendors/froala/js/froala_editor.min.js') }}

	{{ Html::script('packages/rubber/admin/vendors/jquery/ui/jquery-ui.min.js') }}


	<script>
		$(document).foundation();
	</script>

	@include('rubber::common.jsTemplate')

</body>
</html>