@include('rubber::common.html_head')
<head>
	@include('rubber::common.html_meta_tags')
	{{ Html::style('packages/rubber/admin/vendors/foundation/css/normalize.css') }}
	{{ Html::style('packages/rubber/admin/vendors/foundation/css/foundation.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/foundation/css/grid-f5.css') }}
	{{ Html::style('packages/rubber/admin/vendors/font-awesome/css/font-awesome.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/minstrap/css/bootstrapdoc.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/chosen/chosen.min.css') }}
	{{ Html::style('packages/rubber/admin/vendors/froala/css/froala_editor.min.css') }}
	{{ Html::asset('packages/rubber/admin/css/rubber-admin.css') }}
	{{ Html::script('packages/rubber/admin/vendors/foundation/js/vendor/custom.modernizr.js') }}
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,600,italic' rel='stylesheet' type='text/css'>
	{{ Html::style('packages/rubber/admin/vendors/jquery/ui/jquery-ui.theme.min.css') }}

	@if(Config::has('rubber/admin::view.append.head'))
		@include(Config::get('rubber/admin::view.append.head'))
	@endif
	@section('header')

	@show
</head>
<body>

	@if(Sentry::check())
		@include('rubber::common.header.header')
	@endif

	<div id="wrapper" class="row">

		<div id="content" class="large-12 columns">
			@include('rubber::common.breadcrumbs')

			@if($errors->any())
				@include('rubber::common.errors', compact('errors'))
			@endif
			@include('rubber::common.messages')

			@yield('content')

		</div>
	</div>

	@if(Sentry::check())
		@include('rubber::common.footer')
	@endif

	{{ Html::script('packages/rubber/admin/vendors/jquery/jquery.min.js') }}
	{{ Html::script('packages/rubber/admin/vendors/foundation/js/foundation.min.js') }}
	{{ Html::script('packages/rubber/admin/vendors/foundation/js/foundation/foundation.section.js') }}
	{{ Html::script('packages/rubber/admin/vendors/chosen/chosen.jquery.min.js') }}
	{{ Html::script('packages/rubber/admin/js/script.js') }}
	{{ Html::script('packages/rubber/admin/vendors/froala/js/froala_editor.min.js') }}

	{{ Html::script('packages/rubber/admin/vendors/jquery/ui/jquery-ui.min.js') }}

	<script>
		// disable tooltip from popping up on touch device
		$(document).foundation( {tooltips: {
			disable_for_touch: true
		}});
		$('.editor').editable({
			inlineMode: false,
			placeholder: 'Type your text here',
			toolbarFixed: true,
			minHeight: 400,
			imageDeleteURL: "{{ route('admin.media.delete')}}",
			imageUploadURL: "{{ route('admin.media.upload') }}",
			mediaManager: false
		});
		// autoclicking the hash link to force deep linking
		if(window.location.hash){
			$('a[href="'+window.location.hash+'"]').click();
		}
	</script>

	@include('rubber::common.jsTemplate')

	@section('bottomScript')
	@show

	@if(Config::has('rubber/admin::view.append.body'))
		@include(Config::get('rubber/admin::view.append.body'))
	@endif
</body>
</html>