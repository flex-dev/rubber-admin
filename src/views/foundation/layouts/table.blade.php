@extends('rubber::layouts.base')
	@section('content')

		@if (array_key_exists('content_header', View::getSections()))
		<header>
			<h2>@yield('content_header')</h2>
		</header>
		@endif

		@yield('messages')
		@yield('table')
	@stop