@extends('rubber::layouts.detail')

	@section('content_header')
		<h2>{{ $navigation->icon_tag }} {{ $navigation->name }}</h2>
	@stop
	@section('detail')
		<div class="row">
			<div class="large-3 columns small-12 columns">
				{{ $navigation->name_with_icon }}
			</div>
			<div class="large-9 columns small-12 columns">
				@include('rubber::navigations.partials.table', ['tree' => 'descendants'])
			</div>
		</div>
	@stop