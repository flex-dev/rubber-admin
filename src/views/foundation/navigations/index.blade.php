@extends('rubber::layouts.table')

	@section('content_header')
		Navigation Builder
	@stop
	@section('table')
		@include('rubber::navigations.partials.table', compact('tree'))
	@stop
