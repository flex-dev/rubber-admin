@extends('rubber::layouts.detail')

	@section('content_header')
		<h2>Navigation Builder: Sort Position</h2>
	@stop
	@section('detail')
		<div class="row">
			<div  class="large-12 columns">
				<div id="tree">
					<ol>
						@foreach($navigations as $nav)
							@include('rubber::navigations.partials.branch', ['leave' => $nav])
						@endforeach
					</ol>
				</div>
			</div>
		</div>
	@stop

	@section('bottomScript')
		@parent

		<script>
		$(function(){
			var list = $("#tree");
			var callbacks ={};
			callbacks.post = "{{ route('admin.navigation.reorder.apply') }}";
			list.sortable({
				items: 'li',
				containment: '#tree',
				handle: '.leave',
				helper: 'clone',
				cursor: 'move',
				placeholder: 'sortable-placeholder',
				forcePlaceholderSize: true,
				dropOnEmpty: true,
				start: function(event, ui){
					list.find('ol').each(function(i, sublist){
						if($(sublist).find('li').length == 0){
							$(sublist).append("<li class='dummy'>drop here</li>");
						}

					});
				},
				stop: function(event, ui){
					 $(".dummy").remove();
				},
				update: function( event, ui ) {
					list.sortable("disable");
					$.post(
						callbacks.post,
						{
							 ids: list.sortable("toArray"),
						},
						function(response){
							list.sortable("enable");
						}
					);
				}
			}).disableSelection();;
		})
		</script>
	@stop