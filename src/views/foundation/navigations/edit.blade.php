@extends('rubber::layouts.form')

	@section('content_header')
		Navigation Builder :
		@if($navigation->is_divider)
			Edit Separator : {{{ $navigation->name }}}
		@else
			Edit menu : {{{ $navigation->name }}}
		@endif

	@stop

	@section('form')
		{{ Form::model($navigation, array( 'route' => array('admin.navigation.update', $navigation->id),  'method' => 'patch' )) }}
			@include('rubber::navigations.partials.form')
		{{ Form::close() }}
	@stop

