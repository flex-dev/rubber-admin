
		<div class="section-container auto" data-section  data-options="deep_linking: true">
			<section>
				<p class="title" data-section-title><a href="#panel1">Basic Information</a></p>
				<div class="content" data-section-content>

					<div class="row">
					@if(empty($navigation) OR !empty($navigation->parent_id))
						<div class="large-6 small-12 columns {{ $errors->has('parent_id') ? 'error' : '' }}">
							{{ Form::label('parent_id', 'Parent Menu') }}
							{{ Form::select('parent_id',  $tree, isset($navigation) ? $navigation->parent_id : null) }}
							{{ Form::error($errors, 'parent_id')}}
						</div>

						<div class="large-6 small-12  columns {{ $errors->has('parent_id') ? 'error' : '' }}">
							<label for="position">
								Position
								{{ Html::help('You may add/move the menu to the position you wish here.') }}
							</label>
							{{ Form::select('position',
								array(
									'At the top'
								),
								array('id' => 'position_dd' ))
							}}
						</div>
					@else
						<div class="large-12 columns">
							{{ Html::alert('This is the root node. You cannot move it to anywhere', 'Notice', 'warning') }}
							{{ Form::hidden('parent_id', 0)}}
						</div>
					@endif

					</div>

					<div class="row">
						<div class="large-6 small-12 columns {{ $errors->has('name') ? 'error' : '' }}">
							{{ Form::label('name', 'Name') }}
							{{ Form::text('name', null, array( 'placeholder' => 'name of the menu')) }}
							{{ Form::error($errors, 'name')}}
						</div>
						<div class="large-6 small-12 columns">
							<label for="icon">
								Icon
								{{ HTML::help('The css class name for the icon to be used along with the menu. You may reference the advance usage from the font-awesome icon guide page.') }}
							</label>
							{{ Form::text('icon', null, array(
									'placeholder' => 'class(es) name of the icon',
									'class' => 'small-9 columns'
								))
							}}
						</div>
					</div>

					<div class="row">
						<div class="large-12 columns">
							{{ Form::label('description', 'Description') }}
							{{ Form::textarea('description', null, array( 'placeholder' => 'short description of this menu')) }}
						</div>
					</div>

					<div class="row">
						<div class="large-4 small-12 columns">
							<label for="is_divider-">Mark as a separator</label>
								{{ Tinker::checkbox('is_divider', 1) }}
							</label>
						</div>
					</div>

					<div class="row">
						<div class="large-12 columns">
						{{ Html::submit() }}
						</div>
					</div>
				</div>
			</section>

			<section>
				<p class="title" data-section-title><a href="#panel1">Links Information</a></p>
				<div class="content" data-section-content>
					<div class="row">
						<div class="large-12 columns">
						{{ Html::alert('If specified, Link to a specific URL will be used instead of Link to a named route', 'Notice', 'warning') }}
						</div>
					</div>

					<div class="row">
						<div class="large-12 columns {{ $errors->has('route_to') ? 'error' : '' }}">
							{{ Form::label('route_to', 'Link to a specific URL') }}
							{{ Form::text('route_to', null, array( 'placeholder' => 'relative or absolute url accepted.')) }}
							{{ Form::error($errors, 'route_to')}}
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns {{ $errors->has('route_name') ? 'error' : '' }}">
							{{ Form::label('route_name', 'Link to a named route') }}
							{{ Form::select('route_name', $routes, null) }}
							{{ Form::error($errors, 'route_name')}}
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							{{ Html::submit() }}
						</div>
					</div>
				</div>
			</section>

		</div>
