	<li {{ $leave->isRoot() ? 'class="locked"' : '' }}>
		<div >
			<div class="leave label secondary">
				{{ Html::icon($leave->icon)}}
				{{{ $leave->name }}} - <small>{{{ $leave->description }}}</small>
			</div>
			<ol>
			@if($leave->children->count() > 0)

				@foreach($leave->children as $nav)
					@include('rubber::navigations.partials.branch', ['leave' => $nav])
				@endforeach

			@endif
			</ol>
		</div>
	</li>