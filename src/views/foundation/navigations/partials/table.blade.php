	<table data-page-size="5">
		<thead>
			<tr>
				<th width="200" data-toggle="true">Name</th>
				<th data-hide="phone">Navigation</th>
				<th  width="200">Link to</th>
				<th data-hide="phone" width="130">Actions</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="4" class="text-right">
					{{ Html::create(' New Menu', route('admin.navigation.create'))}}				</td>
			</tr>
		</tfoot>
		<tbody>
		@unless($tree)
		<tr>
			<td colspan="4">No menu added to the tree.</td>
		</tr>
		@else
		@foreach($tree as $leave)
		<tr>
			<td>
				@if($leave->depth>0)
				<span class="tree-depth">
					@for($i=1; $i<=$leave->depth; $i++)
					{{ HTML::icon('fa-caret-right') }}
					@endfor
				</span>
				@endif
				{{ HTML::icon($leave->icon) }}
				{{{ $leave->name }}}
			</td>
			@unless($leave->is_divider)
				<td>
					@if($leave->description)
						<nav class="breadcrumbs has-tip tip-top" data-tooltip title="{{{ $leave->description }}}">
					@else
						<nav class="breadcrumbs">
					@endif
							@foreach($leave->getAncestorsAndSelf() as $node)
							<span class="secondary">
								{{ Html::icon($node->icon) }} {{{ $node->name }}}
							</span>
							@endforeach
						</nav>
				</td>
				<td>
					{{ Html::icon('icon-link') }}
					@if(!empty($leave->route_to))
						<a href="{{ url($leave->route_to) }}" target="_blank" class="has-tip tip-top" data-tooltip title="URL: {{  url($leave->route_to) }}">{{ $leave->route_to }}</a>
					@elseif(Route::getRoutes()->hasNamedRoute($leave->route_name))
						<a href="{{ route($leave->route_name) }}" class="has-tip tip-top" data-tooltip title="URL: {{  route($leave->route_name) }}">{{ $leave->route_name }}</a>
					@else
						<a href="#"><del>{{ $leave->route_name }}</del> Route Not Exists</a>
					@endif
				</td>
			@else
				<td colspan="2">separator nodes</td>
			@endunless
			<td>
				{{ Html::edit( null, route('admin.navigation.edit', $leave->id)) }}
				{{ Html::delete( null, route('admin.navigation.delete', $leave->id)) }}
			</td>
		</tr>
		@endforeach
		@endunless
		</tbody>
		</table>