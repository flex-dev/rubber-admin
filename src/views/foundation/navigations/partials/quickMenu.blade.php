	<div id="quickMenuModel" class="reveal-modal small">
		<h2>Add page to menu</h2>
		{{ Form::open(array( 'route' => 'admin.navigation.store', 'method' => 'post', 'id' => 'quickMenuForm'))}}
		{{ Form::hidden('route_name', Route::currentRouteName() ) }}
		{{ Form::hidden('route_to',  Request::path()) }}
		<div class="row">
			<div class="large-12 columns {{ $errors->has('parent_id') ? 'error': '' }}">
			{{ Form::label('parent_id', 'Add to ')}}
			{{ Form::select('parent_id', $tree, null, array( 'class' => 'custom' )) }}
			{{ Form::error($errors, 'parent_id') }}
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns {{ $errors->has('name') ? 'error': '' }}">
			{{ Form::label('name', 'Link name ')}}
			{{ Form::text('name', null, array( 'placeholder' => 'name of the menu')) }}
			{{ Form::error($errors, 'name') }}
			</div>
			<div class="large-6 columns">
			{{ Form::label('icon', 'Classes for icon ')}}
			{{ Form::text('icon', null, array( 'placeholder' => 'fa-xxx')) }}
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
			{{ Html::submit() }}
			</div>
		</div>
		{{ Form::close() }}
		<a class="close-reveal-modal">&#215;</a>
	</div>