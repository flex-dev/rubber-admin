@extends('rubber::layouts.form')

	@section('content_header')
		Navigation Builder : Create Menu
	@stop

	@section('form')
		{{ Form::open(array( 'route' => 'admin.navigation.store', 'method' => 'post')) }}
			@include('rubber::navigations.partials.form')
		{{ Form::close() }}
	@stop

