@extends('rubber::layouts.blank')

	@section('content')
	<div id="loginForm" class="panel">
		<h1>Lost Password</h1>
		@if($errors->any())
			@include('rubber::common.errors', compact('errors'))
		@endif
		<div class="bs-callout bs-callout-info">
			<p>Enter you account's email address to reset the account password.</p>
		</div>
		{{ Form::open(array( 'route' => 'admin.user.lost-password', 'method' => 'post' ))}}
			<div class="row collapse {{ $errors->has('email') ? 'error' : '' }}">
				<div class="small-10 columns">
					{{ Form::email('email', null, array( 'placeholder' => 'Email Address')) }}
				</div>
				<div class="small-2 columns">
					<span  class="postfix  has-tip tip-top" data-tooltip title="An email address registered as a user account.">{{ Html::icon('fa-envelope') }}</span>
				</div>
			</div>

			{{ Form::submit('Request Password Reset', array( 'class' => ' tiny button' )) }}
			<a href="{{ route('admin.login') }}" class="tiny button secondary"><i class="fa fa-sign-out"></i> Back to Login</a>

		{{ Form::close() }}
	</div>
	@stop