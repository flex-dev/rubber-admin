@extends('rubber::layouts.base')

	@section('content')

	<div class="row">
		<div class="large-9 columns">
			<header><h2><i class="fa fa-lg fa-users"></i> User Groups</h2></header>
			<table>
				<thead>
				<tr>
					<th width="">Group</th>
					<th class="text-center">Super User</th>
					<th width="240">Manage</th>
					<th width="180">Action</th>
				</tr>
				</thead>
				<tbody>
				@foreach($groups as $gr)
					<tr>
						<td>
							<strong>{{{ $gr->name }}}</strong><br>
							<small>{{{ $gr->description }}}</small>
						</td>
						<td class="text-center">
							@if($gr->superuser())
								<span class="label primary circular"><i class="fa fa-check fa-lg"></i> </span>
							@endif
						</td>
						<td>
							<a href="{{ route('admin.group.show', $gr->id) }}" class="tiny button secondary"><i class="fa fa-lg fa-users"></i> {{ $gr->total_users }} Users</a>
							<a href="{{ route('admin.group.permissions', $gr->id) }}" class="tiny button secondary">
								@if($gr->permissions)
								<i class="fa fa-lg fa-lock"></i>
								@else
								<i class="fa fa-lg fa-unlock"></i>
								@endif
							 Permissions</a>
						</td>
						<td>
							{{ HTML::edit(null, route('admin.group.edit', $gr->id)) }}
							@if(!$gr->superuser())
							{{ HTML::delete(null, route('admin.group.delete', $gr->id)) }}
							@endif
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>

			{{ $groups->links() }}

		</div>

		<div class="large-3 columns">
		@if(!empty($group))
			@include('rubber::groups.edit')
		@else
			@include('rubber::groups.create')
		@endif
		</div>
	</div>
	@stop