		<header><h2><i class="fa fa-plus-square fa-lg"></i> New Group</h2></header>
		<div class="panel">
			{{ Form::open(array(
				'route' => array('admin.group.store'),
				'method' => 'post'))
			}}
			@include('rubber::groups.partials.form')
			{{ Form::close() }}
		</div>