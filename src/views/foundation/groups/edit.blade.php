
		<header><h2><i class="fa fa-pencil fa-lg"></i> Edit Group</h2></header>
		<div class="panel">
			{{ Form::model($group, array(
				'route' => array('admin.group.update', $group->id),
				'method' => 'patch'))
			}}
			@include('rubber::groups.partials.form')
			{{ Form::close() }}
		</div>