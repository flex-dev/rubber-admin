@extends('rubber::layouts.base')
	@section('content')

	<header>
		<h2>
		<i class="fa fa-lg fa-gavel"></i> User Group: {{{ $group->name }}} - Permissions.</h2>
	</header>
	{{ Form:: open(array( 'route' => array('admin.group.permission.grant', $group->id), 'method' => 'post')) }}

	<div class="row" id="group_permissions">
		<div class="large-12 columns">
			<div class="section-container vertical-tabs" data-section="vertical-tabs">
			<?php $i = 0; ?>
			@foreach($routes as $package => $routesCollection)

				<section>
					<p class="title" data-section-title>
						<a href="#"><strong>{{ $package ? $package : 'Global Namespace' }}</strong></a>
					</p>
					<div class="content" data-section-content>
						<div class="section-container accordion" data-section="accordion" data-options="one_up: true">

						@foreach($routesCollection as $controller => $route)

							<section class="active">

								<div class="content" data-section-content>
									<div class="row">
										<div class="large-10 small-8 columns">
											<h3 class="title" data-section-title>
												<a href="#"><strong>{{{ $controller }}}</strong></a>
											</h3>
										</div>
										<div class="large-2 small-4 columns">
											<div style="width: 100px; padding: 7px;" class="right">
												{{ Tinker::checkbox('toggleAll['.$controller.']', 1, false, array('class' => 'toggleAll')) }}
											</div>
										</div>
									</div>
									<table class="responsive">
									<thead>
									<tr>
										<th width="150"></th>
										<th>Name / Path</th>
										<th width="100">Method</th>
										<th width="100">{{ HTML::icon('fa-check-square') }} Allowed</th>
									</tr>
									</thead>
									<tbody>
									@foreach($route as $path)
										<?php
											$i++;
											$hasPermission = $group->hasAccess($path->name) ;
											$bible = $bibles->first(function($idx, $item) use ($path){
												return ($item->method == strtolower($path->methods[0])) && ($item->route == $path->name);
											});
										?>

										<tr>
											<td>
												@foreach($path->methods as $method)
												<small class="label secondary">{{{ $method }}}</small>
												@endforeach
											</td>
											<td>
												@if($bible)
												<strong>{{{ $bible->name }}}</strong><br>
												@endif
												<small>{{{ $path->name }}}</small><br>
												<small>{{{ $path->path }}}</small>
											</td>
											<td>{{{ $path->function }}}</td>
											<td>{{ Tinker::checkbox("permissions[$path->name]", 1, $hasPermission, array('id' => 'check-'.$i)) }}</td>
										</tr>
									@endforeach
									</tbody>
									</table>
								</div>
							</section>

						@endforeach
						</div>
					</div>

				</section>
			@endforeach

			</div>
		</div>
	</div>

	<div class="row">
		<div class="large 12 columns">
			<div class="text-center">
				{{ Html::submit('Update Permissions') }}
			</div>
		</div>
	</div>

	{{ Form::close() }}

	@stop
