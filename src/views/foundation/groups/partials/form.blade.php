
			<div class="row">
				<div class="large 12 columns">
				{{ Form::label('name', 'Name') }}
				{{ Form::text('name', null, array( 'placeholder' => 'The name of the group' )) }}
				</div>
			</div>
			<div class="row">
				<div class="large 12 columns">
				{{ Form::label('description', 'Description') }}
				{{ Form::textarea('description', null, array( 'rows' => '3' )) }}
				</div>
			</div>
			<div class="row">
				<div class="large 6 columns">
				{{ Form::label('superuser', 'Super User Group') }}
				@if(!empty($group))
				{{ Tinker::checkbox('superuser', 1, $group->hasAccess('superuser')) }}
				@else
				{{ Tinker::checkbox('superuser', 1) }}
				@endif
				</div>
			</div>
			<div class="row">
				<div class="large 12 columns">
					{{ Html::submit('Save')}}
					@unless(empty($group))
					<a href="{{ route('admin.group.list') }}" class="button tiny secondary"><i class="fa fa-times fa-lg"></i> Cancel</a>
					@endunless
				</div>
			</div>
