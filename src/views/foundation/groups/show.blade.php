@extends('rubber::layouts.detail')

	@section('content_header')
		Group: {{{ $group->name }}}
	@stop

	@section('detail')
		<div class="row">

			<div class="large-4 columns">
				<table>
					<thead>
						<tr>
							<th colspan="3'"><h3>Members</h3></th>
						</tr>
						<tr>
							<th width="60"></th>
							<th>Name</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
					@unless($users)
					<tr>
						<td colspan="3">No users has been added to the group</td>
					</tr>
					@else
					@foreach($members as $user)
						<tr>
							<td><img src="{{ $user->avatar_image_path }}"></td>
							<td>
								{{{ $user->full_name_or_email }}}<br>
								<small>{{{ $user->email }}}</small>
							</td>
							<td>
								{{ Html::delete( null, route('admin.group.user.remove', array($group->id, $user->id)))}}
							</td>
						</tr>
					@endforeach
					@endunless
					</tbody>
				</table>

				<div class="panel">
					{{ Form::open(array(
						'route'=>array('admin.group.user.add', $group->id),
						'method' => 'POST',
						))
					}}
					<div class="row collapse">
						<div class="large-8 columns">
							{{ Form::select('user_id', $users, null, array( 'class' => 'chosen' )) }}
						</div>
						<div class="large-4 columns">
							<span class="suffix">
								<button class="button tiny success">
									<i class="fa fa-lg fa-check"></i> Add
								</button>
							</span>
						</div>
					</div>
					{{ Form::close() }}
				</div>

			</div>

			<div class="large-8 columns">
				<table>
					<thead>
						<tr>
							<th colspan="2'"><h3>Member in this group can access:</h3></th>
						</tr>
						<tr>
							<th width="150">Method</th>
							<th>Name</th>
						</tr>
					</thead>
					<tbody>
					@unless($group->permissions)
					<tr>
						<td>No permission granted to this group</td>
						<td>{{ Html::edit('Set Permissions', route('admin.group.permissions', $group->id), array('icon' => 'icon-lock icon-large')) }}</td>
					</tr>
					@else
					@foreach($group->permissions as $permission=>$allowed)
						<tr>
							<td>
							@if( $definedRoutes->hasNamedRoute($permission))
								<?php $route = $definedRoutes->getByName($permission); ?>

								@foreach($route->getMethods() as $method)
								<span class="label secondary">{{ $method }}</span>
								@endforeach
							@else
								Route missing
							@endif
							</td>
							<td>
								{{ $permission }}<br>
								@if( $definedRoutes->hasNamedRoute($permission))
									<small>{{ $route->getPath() }}</small>
								@else
									<s>{{ $permission }}</s>
								@endif
							</td>
						</tr>
					@endforeach
					@endunless
					</tbody>
				</table>
			</div>


		</div>

	@stop