@extends('rubber::layouts.base')

	@section('content')
		<div class="row">
			<div class="large-9 columns">
				<header>
					<h2><i class="fa fa-lg fa-code-fork"></i> Accessible Routes</h2>
				</header>
				<table class="filterable">
					<thead>
						<tr>
							<th class="text-right" width="100">Method</th>
							<th width="220">Route</th>
							<th>Description</th>
						</tr>
					</thead>
					<tbody>
						@foreach($routes as $route)
						<tr data-name="{{{ $route->name }}}">
							<td class="text-right"><span class="label secondary">{{{ $route->method }}}</span></td>
							<td>{{{ $route->route }}}</td>
							<td>
								{{ Form::model($route, array( 'url' => $route->update_url, 'class' => 'bible', 'method' => 'patch' )) }}
								<div class="row">
									<div class="large-12 columns">
										<input name="name" type="text" class="has-tip tip-top" title="click to edit, data saved on blured" value="{{{ $route->name }}}" {{ $route->name ? 'disabled' : '' }}>
									</div>
								</div>
								{{ Form::close() }}
							</td>
						</tr>
						@endforeach
						</tbody>
				</table>
			</div>
			<div class="large-3 columns">
				<header><h2><i class="fa fa- fa-search"></i> Filter</h2></header>
				{{ Form::open( array('id' => 'bibleFiltering') ) }}
				<div class="panel">
					<div class="row">
						<div class="large-12 columns">
							<label for="">Route Name</label>
							{{ Form::text('query', null, array( 'id' => 'input-filter' ))}}
						</div>
					</div>
				</div>
				{{ Form::close() }}

				<div class="panel">
					<div class="row">
						<div class="large-12 columns">
							{{ Form::open( array('route' => 'admin.bible.sync', 'method' => 'post')) }}
							<button type="submit" class="button tiny primary-dark right radius"><i class="fa fa-lg fa-code-fork"></i> Synchronize</button>
							{{ Form::close() }}
						</div>
						<div class="large-12 columns">
							{{ Form::open( array('route' => 'admin.bible.purge', 'method' => 'delete', 'class' => 'delete-form')) }}
							<button type="submit" class="button tiny alert right radius"><i class="fa fa-lg fa-trash"></i> World Purge</button>	&nbsp;
							{{ Form::close() }}
						</div>
					</div>
				</div>

			</div>
		</div>

	@stop

	@section('bottomScript')
	<script>
		$(function(){
			$("body").on('blur', '.bible input', function(ev){
				ev.preventDefault();
				var form = $(this).closest('form');
				var url = form.attr('action');
				(function(form, input){
					var data = form.serialize();
					input.disabled = true;
					form.addClass('saving');
					$.post(url, data, function(response){
						form.removeClass('saving');
					});
				})(form, this);
			}).on('dblclick', '.bible input', function(ev){
				ev.preventDefault();
				ev.currentTarget.disabled = !ev.currentTarget.disabled;
				ev.currentTarget.focus();
				return;
			});
		});
	</script>
	@stop
