@extends('rubber::layouts.blank')

	@section('content')
	<div id="loginForm" class="panel">
		<div class="row">
			<div class="large-12 columns">
				<h1>Set Password</h1>
				@if($errors->any())
					@include('rubber::common.errors', compact('errors'))
				@endif
				<div class="bs-callout bs-callout-info">
					<p>Enter you reset password token, and type your new password</p>
				</div>
			</div>
		</div>
		{{ Form::open(array( 'route' => array('admin.user.reset-password', $token), 'method' => 'post' ))}}
			<div class="row">
				<div class="large-12 columns">
					{{ Form::label('token', 'Token')}}
					{{ Form::textarea('token', $token)}}
				</div>
			</div>

			<div class="row">
				<div class="large-12 columns">
					{{ Form::label('password', 'New Password')}}
					{{ Form::password('password', array( 'placeholder' => 'type your new password here'))}}
				</div>
			</div>

			<div class="row">
				<div class="large-12 columns">
					{{ Form::submit('Reset Password', array( 'class' => ' tiny button' )) }}
				</div>
			</div>
		{{ Form::close() }}
	</div>
	@stop