<a href="#" data-dropdown="iconPicker" class="tiny button dropdown secondary">Choose...</a>
<ul id="iconPicker" class="f-dropdown content" data-dropdown-content>
		<li>
			<i class="fa fa-fw"></i>
			fa-glass
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-music
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-search
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-envelope-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-heart
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-star
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-star-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-user
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-film
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-th-large
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-th
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-th-list
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-check
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-times
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-search-plus
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-search-minus
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-power-off
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-signal
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-cog
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-trash-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-home
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-file-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-clock-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-road
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-download
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-arrow-circle-o-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-arrow-circle-o-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-inbox
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-play-circle-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-repeat
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-refresh
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-list-alt
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-lock
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-flag
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-headphones
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-volume-off
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-volume-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-volume-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-qrcode
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-barcode
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-tag
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-tags
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-book
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-bookmark
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-print
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-camera
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-font
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-bold
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-italic
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-text-height
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-text-width
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-align-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-align-center
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-align-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-align-justify
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-list
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-outdent
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-indent
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-video-camera
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-picture-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-pencil
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-map-marker
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-adjust
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-tint
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-pencil-square-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-share-square-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-check-square-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-arrows
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-step-backward
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-fast-backward
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-backward
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-play
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-pause
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-stop
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-forward
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-fast-forward
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-step-forward
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-eject
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-chevron-left
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-chevron-right
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-plus-circle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-minus-circle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-times-circle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-check-circle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-question-circle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-info-circle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-crosshairs
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-times-circle-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-check-circle-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-ban
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-left
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-right
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-up
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-down
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-share
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-expand
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-compress
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-plus
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-minus
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-asterisk
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-exclamation-circle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-gift
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-leaf
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-fire
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-eye
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-eye-slash
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-exclamation-triangle
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-plane
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-calendar
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-random
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-comment
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-magnet
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-chevron-up
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-chevron-down
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-retweet
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-shopping-cart
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-folder
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-folder-open
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrows-v
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrows-h
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-bar-chart-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-twitter-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-facebook-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-camera-retro
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-key
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-cogs
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-comments
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-thumbs-o-up
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-thumbs-o-down
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-star-half
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-heart-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-sign-out
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-linkedin-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-thumb-tack
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-external-link
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-sign-in
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-trophy
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-github-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-upload
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-lemon-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-phone
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-square-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-bookmark-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-phone-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-twitter
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-facebook
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-github
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-unlock
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-credit-card
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-rss
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-hdd-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-bullhorn
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-bell
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-certificate
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-hand-o-right
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-hand-o-left
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-hand-o-up
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-hand-o-down
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-circle-left
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-circle-right
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-circle-up
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrow-circle-down
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-globe
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-wrench
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-tasks
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-filter
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-briefcase
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-arrows-alt
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-users
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-link
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-cloud
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-flask
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-scissors
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-files-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-paperclip
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-floppy-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-bars
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-list-ul
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-list-ol
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-strikethrough
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-underline
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-table
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-magic
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-truck
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-pinterest
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-pinterest-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-google-plus-square
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-google-plus
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-money
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-caret-down
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-caret-up
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-caret-left
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-caret-right
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-columns
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-sort
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-sort-asc
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-sort-desc
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-envelope
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-linkedin
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-undo
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-gavel
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-tachometer
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-comment-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-comments-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-bolt
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-sitemap
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-umbrella
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-clipboard
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-lightbulb-o
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-exchange
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-cloud-download
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-cloud-upload
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-user-md
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-stethoscope
		</li>

		<li class="col-md-4 col-sm-6 col-lg-3">
			<i class="fa fa-fw"></i>
			fa-suitcase
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-bell-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-coffee
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-cutlery
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-file-text-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-building-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-hospital-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-ambulance
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-medkit
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-fighter-jet
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-beer
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-h-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-plus-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-double-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-double-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-double-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-double-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-angle-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-desktop
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-laptop
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-tablet
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-mobile
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-circle-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-quote-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-quote-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-spinner
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-circle
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-reply
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-github-alt
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-folder-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-folder-open-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-smile-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-frown-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-meh-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-gamepad
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-keyboard-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-flag-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-flag-checkered
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-terminal
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-code
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-reply-all
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-mail-reply-all
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-star-half-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-location-arrow
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-crop
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-code-fork
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-chain-broken
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-question
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-info
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-exclamation
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-superscript
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-subscript
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-eraser
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-puzzle-piece
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-microphone
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-microphone-slash
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-shield
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-calendar-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-fire-extinguisher
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-rocket
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-maxcdn
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-chevron-circle-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-chevron-circle-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-chevron-circle-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-chevron-circle-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-html5
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-css3
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-anchor
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-unlock-alt
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-bullseye
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-ellipsis-h
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-ellipsis-v
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-rss-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-play-circle
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-ticket
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-minus-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-minus-square-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-level-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-level-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-check-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-pencil-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-external-link-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-share-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-compass
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-caret-square-o-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-caret-square-o-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-caret-square-o-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-eur
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-gbp
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-usd
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-inr
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-jpy
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-rub
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-krw
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-btc
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-file
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-file-text
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-sort-alpha-asc
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-sort-alpha-desc
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-sort-amount-asc
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-sort-amount-desc
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-sort-numeric-asc
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-sort-numeric-desc
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-thumbs-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-thumbs-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-youtube-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-youtube
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-xing
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-xing-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-youtube-play
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-dropbox
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-stack-overflow
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-instagram
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-flickr
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-adn
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-bitbucket
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-bitbucket-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-tumblr
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-tumblr-square
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-long-arrow-down
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-long-arrow-up
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-long-arrow-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-long-arrow-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-apple
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-windows
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-android
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-linux
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-dribbble
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-skype
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-foursquare
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-trello
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-female
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-male
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-gittip
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-sun-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-moon-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-archive
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-bug
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-vk
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-weibo
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-renren
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-pagelines
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-stack-exchange
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-arrow-circle-o-right
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-arrow-circle-o-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-caret-square-o-left
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-dot-circle-o
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-wheelchair
		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-vimeo-square

		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-try

		</li>

		<li>
			<i class="fa fa-fw"></i>
			fa-plus-square-o
		</li>
	</ul>