<footer id="footer" >
	<div class="row container text-center">
		<p>{{  sprintf(Config::get('rubber/admin::settings.footer.text', '&copy; copyright'), date('Y'))  }}</p>
	</div>
</footer>