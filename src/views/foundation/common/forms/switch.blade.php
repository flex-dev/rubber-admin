
	<label class="rubber-checkbox">
		{{ Form::hidden($name, 0)}}
		{{ Form::checkbox($name, $value, Input::old($name, $checked), $options) }}
		<span></span>
	</label>