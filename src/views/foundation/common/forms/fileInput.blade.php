 <div class="row collapse filePlaceholder">
	<div class="small-10 columns">
		<input type="text" class="uploadPlaceholder" value="select a file" readonly>
	</div>
	<div class="small-1 columns filePreviewer">
		<span class="postfix">
			{{ Html::icon('fa-ellipsis-h') }}
			@unless(empty($options['src']))
				<img src="{{ $options['src'] ? $options['src'] : '' }}" class="filePreview has-tip tip-top" data-tooltip title="click to view picture">
			@else
				<img  class="filePreview has-tip tip-top" data-tooltip  title="click to view picture">
			@endunless
		</span>
	</div>
	<div class="small-1 columns fileRemove">
		<span class="postfix button  error has-tips tip-top" data-tooltip title="remove the selected file">{{ Html::icon('fa-times') }}</span>
	</div>
	{{ Form::file($name, $options )}}
</div>