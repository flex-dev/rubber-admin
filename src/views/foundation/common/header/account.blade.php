<ul id="account-menu" class="right">
	<li class="has-dropdown">
		<?php $user = Sentry::getUser(); ?>
		<a href="#" id="account-avatar">
			<strong>{{ $user->full_name_or_email }}</strong>
			<img src="{{ $user->avatar_image_src }}" class="hide-for-small">
		 </a>
		<ul class="dropdown">
			<li>
				<a href="{{ route('admin.self.edit') }}">Edit Profile {{ Html::icon('fa-pencil fa-large') }} </a>
			</li>
			<li>
				<a href="{{ route('admin.self.edit') }}#password">Change Password {{ Html::icon('fa-lock fa-large') }} </a>
			</li>
			<li>
				<a href="{{ route('admin.logout') }}">Log Out {{ Html::icon('fa-unlock fa-large') }} </a>
			</li>
		</ul>
	</li>
</ul>