 <div id="header" class="contain-to-grid">
	 <nav class="top-bar"  data-options="mobile_show_parent_link: true">
		<ul class="title-area">
			<li class="name">
			<h1>
				<a href="{{ URL::route('admin.home') }}">
				{{ HTML::icon('fa-quote-left') }}
				{{{ empty($title) ?  Config::get('rubber/admin::settings.application_name', 'No title') : $title }}}
				</a>
			</h1>
			</li>
			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		@section('navigation')
			@if(Config::get('rubber/admin::view.navigation'))
				@include(Config::get('rubber/admin::view.navigation'))
			@else
				@include('rubber::common.header.navigation')
			@endif
			<section class="top-bar-section">
				@include('rubber::common.header.account')
			</section>
		@show
	</nav>
</div>
