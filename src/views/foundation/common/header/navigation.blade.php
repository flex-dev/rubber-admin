
	<section class="top-bar-section">
		@foreach($navigations as $nav)
		<ul class="left">
			<li {{ !$nav->children->isEmpty() ? 'class="has-dropdown"' : '' }}>
				@if(!empty($nav->route_to))
					<a href="{{ URL::to($nav->route_to) }}">
				@else
					@if(Route::getRoutes()->hasNamedRoute($nav->route_name))
						<a href="{{ URL::route($nav->route_name) }}">
					@else
						<a href="#" title="route not exists">
					@endif
				@endif
				{{ HTML::icon($nav->icon) }}
				{{{ $nav->name }}}
				</a>
				@if(!$nav->children->isEmpty())
					@include('rubber::common.header.subnavigation', array( 'navigations' => $nav->children ))
				@endif
		</li>
		</ul>
		@endforeach
	</section>