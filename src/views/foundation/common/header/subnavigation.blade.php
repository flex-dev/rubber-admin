			<ul class="dropdown">
				@foreach($navigations as $nav)
				@if($nav->is_divider)
					<li class="divider"></li>
				@else
					<li {{ !$nav->children->isEmpty() ? 'class="has-dropdown"' : '' }}>
						@if(!empty($nav->route_to))
							<a href="{{ URL::to($nav->route_to) }}">
						@else
							@if(!empty($nav->route_name))
								@if(Route::getRoutes()->hasNamedRoute($nav->route_name))
									<a href="{{ URL::route($nav->route_name) }}">
								@else
									<a href="#" title="route not exists">
								@endif
							@else
								<a href="#">
							@endif
						@endif
							{{ HTML::icon($nav->icon) }}
							{{{ $nav->name }}}
						</a>
						@if(!empty($nav->children))
							@include('rubber::common.header.subnavigation', array('navigations' => $nav->children))
						@endif
					</li>
				@endif
				@endforeach
			</ul>
