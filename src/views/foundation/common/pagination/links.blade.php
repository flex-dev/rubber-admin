@unless(empty($paginator))
<div class="panel ">
	<small>page {{ $paginator->getCurrentPage() }} of {{ $paginator->getLastPage() }}.
			showing records {{$paginator->getFrom() }} - {{ $paginator->getTo() }} from the total of {{ $paginator->getTotal() }} records</small>
</div>
@endunless

@unless($paginator->getLastPage() <= 1)
<div class="pagination">
	<ul class="button-group radius">
		{{ with(new Rubber\Admin\FoundationPaginator($paginator))->render() }}
	</ul>
</div>
@endunless
