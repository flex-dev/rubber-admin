	@unless(empty($filters))

	 <ul class="button-group">
	 <li>
		<a href="#" data-dropdown="tableFilter" class="button small secondary dropdown">Filter By</a>
		<ul id="tableFilter" class="f-dropdown" data-dropdown-content>
			@unless(empty($filters['default']))
			<li>
			{{ Html::button('anchor', Html::icon('fa fa-check').' Active', array(
					'href' => $filters['default'],
					'class' => 'small'
				))
			}}
			</li>
			@endunless

			@unless(empty($filters['all']))
			<li>
				{{ Html::button('anchor', Html::icon('fa fa-list').' All', array(
						'href' => $filters['all'],
						'class' => 'small'
					))
				}}
			</li>
			@endunless

			@unless(empty($filters['trashed']))
			<li>
				{{ Html::button('anchor', Html::icon('fa-trash-o').' Deleted', array(
						'href' =>  $filters['trashed'],
						'class' => 'small'
					))
				}}
			</li>
			@endunless

		</ul>
		<li>
	<ul>

	@unless(empty($filters['others']))
		@foreach($filters['others'] as $filter_type=>$others)
		<ul class="button-group">
		 <li>
			<a href="#" data-dropdown="{{{  camel_case($filter_type) }}}" class="button small secondary dropdown">{{{ $filter_type }}}</a>
			<ul id="{{{ camel_case($filter_type) }}}" class="f-dropdown" data-dropdown-content>
			@foreach($others as $k=>$filter)
				<li>
					{{ Html::button('anchor', (empty($filter['icon']) ? null : Html::icon($filter['icon'])).' '.$k, array(
							'href' =>  empty($filter['href']) ? '#' : $filter['href'],
							'class' =>  'small '.empty($filter['class']) ? null : $filter['class']
						))
					}}
				</li>
			@endforeach
			</ul>
		</li>
		</ul>
		@endforeach
	@endunless

	@endunless