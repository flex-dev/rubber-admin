
	<title>{{ empty($title) ?  Config::get('settings.title', 'No title') : $title }}</title>

	<!-- Viewport metatags -->
	{{ HTML::meta('HandheldFriendly', 'true') }}
	{{ HTML::meta('MobileOptimized', Config::get('settings.meta.mobileOptimized', 480 )) }}
	{{ HTML::meta('viewport', Config::get('settings.meta.viewport', 'width=device-width' )) }}
	<!-- iOS webapp metatags -->
	{{ HTML::meta('apple-mobile-web-app-capable', 'yes') }}
	{{ HTML::meta('apple-mobile-web-app-status-bar-style', 'black') }}