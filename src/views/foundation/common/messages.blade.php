
	@if($messages->any())
		@foreach($messages->getMessages() as $type=>$msgs)
			@foreach($msgs as $msg)
				<?php @list($type, $msg) = explode(':', $msg, 2); ?>
				@if(empty($msg))
				    {{ Html::alert($type, null)}}
				@else
				    {{ Html::alert($msg, null, $type)}}
				@endif
			@endforeach
		@endforeach
	@endif