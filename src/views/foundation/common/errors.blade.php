@if($errors->any())
	<div class="bs-callout bs-callout-danger">
		<h4>Some errors has occured during the operation.</h4>
		<ul>
		@foreach($errors->all() as $msg)
			<li>{{ $msg }}</li>
		@endforeach
		</ul>
	</div>
@endif