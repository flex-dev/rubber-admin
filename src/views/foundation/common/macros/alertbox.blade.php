<div class="bs-callout bs-callout-{{ $class ? $class : 'info' }}">
	<h4>{{ $title ? $title : '' }}</h4>
	<p>{{ $text }}</p>
</div>