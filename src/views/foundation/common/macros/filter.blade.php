<a href="#" data-dropdown="{{ $id }}" class="button dropdown {{ $button ? $button : 'secondary' }}">{{ $name }}</a>
<ul id="{{ $id }}" class="f-dropdown {{ $size ? $size : '' }} {{ $hasContent ? 'data-dropdown-content' : '' }}">
@foreach($filters as $text => $url)
	  <li><a href="{{ $url }}">{{ $text }}</a></li>
@endforeach
</ul>