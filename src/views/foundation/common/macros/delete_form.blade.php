{{ Form::open(array('url' => $url, 'method' => 'delete', 'class' => $attributes['delete_form_class'] ))}}
    <button class="{{ $attributes['delete_button_class'] }}">{{ HTML::icon( $attributes['delete_button_icon_class'] )}}{{ $text }}</button>
{{ Form::close() }}