<object data="{{ URL::asset($file) }}" type="application/x-shockwave-flash"
	@unless(empty($params['attributes']))
		@foreach($params['attributes'] as $attr=>$value)
		{{ $attr }}="{{ $value }}"
		@endforeach
	@endunless
>
	@unless(empty($params['options']))
		@foreach($params['options'] as $param=>$value)
		<param name="{{ $param }}" value="{{ $value }}">
		@endforeach
	@endunless
	<param name="movie" value={{ URL::asset($file) }}>
</object>