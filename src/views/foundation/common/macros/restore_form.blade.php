{{ Form::open(array('url' => $url, 'method' => 'patch', 'class' => $attributes['restore_form_class'] ))}}
    <button class="{{ $attributes['restore_button_class'] }}">{{ HTML::icon( $attributes['restore_button_icon_class'] )}} Restore</button>
{{ Form::close() }}