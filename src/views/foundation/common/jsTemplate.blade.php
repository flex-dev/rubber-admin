
		<div id="deleteFormModal" class="reveal-modal medium">
			<header><h2 data-content="title">Confirm Delete</h2></header>
			<p class="lead" data-content="message">This action requires a confirmation. Do you really want to delete this data ? If the data is in the trashed bin, it'll be deleted permanently.</p>
			<a class="close-reveal-modal"></a>
			{{ HTML::button('button', 'Yes', array(
				'class' => Config::get('rubber/admin::styles.delete_button_class', 'button' ),
				'id' => 'deleteConfirm' ))
			 }}
			{{ HTML::button('button', 'No', array(
				'class' => Config::get('rubber/admin::styles.cancel_button_class', 'button' ),
				'id' => 'deleteCancel' ))
			}}
		</div>

		<div id="restoreFormModal" class="reveal-modal medium">
			<div class="header"><h2 data-content="title">Confirm Restoration</h2></div>
			<p class="lead" data-content="message">This action requires a confirmation. Do you really want to restore this data ?</p>
			<a class="close-reveal-modal"></a>
			{{ HTML::button('button', 'Yes', array(
				'class' => Config::get('rubber/admin::styles.restore_button_class', 'button' ),
				'id' => 'restoreConfirm' ))
			 }}
			{{ HTML::button('button', 'No', array(
				'class' => Config::get('rubber/admin::styles.cancel_button_class', 'button' ),
				'id' => 'restoreCancel' ))
			}}
		</div>

		<div id="previewModel" class="reveal-modal medium"></div>
