@extends('rubber::layouts.blank')

	@section('content')
	<div id="loginForm" class="panel">
		<h1>Admin Installation</h1>
		<p class="panel">Please enter an email address and a password for the initial administrator account</p>
		@if($errors->any())
			@include('rubber::common.errors', compact('errors'))
		@endif
		{{ Form::open(array( 'route' => 'admin.install', 'method' => 'post' ))}}

			<div class="row collapse {{ $errors->has('email') ? 'error' : '' }}">
				<div class="small-10 columns">
					{{ Form::email('email', null, array( 'placeholder' => 'Email', 'autocomplete' => 'off')) }}
				</div>
				<div class="small-2 columns">
					<span  class="postfix  has-tip tip-top" data-tooltip title="An email address for logging in to the new account">{{ Html::icon('fa-key') }}</span>
				</div>
			</div>

			<div class="row collapse {{ $errors->has('password') ? 'error' : '' }}">
				<div class="small-10 columns">
					{{ Form::password('password', array( 'placeholder' => 'Password', 'autocomplete' => 'off')) }}
				</div>
				<div class="small-2 columns">
					<span class="postfix has-tip tip-top" data-tooltip title="The password accompany with the account">{{ Html::icon('fa-asterisk') }}</span>
				</div>
			</div>

			<div class="row collapse {{ $errors->has('group') ? 'error' : '' }}">
				<div class="small-10 columns">
					{{ Form::text('group', 'Administrator', array( 'placeholder' => 'Group Name')) }}
				</div>
				<div class="small-2 columns">
					<span class="postfix has-tip" data-tooltip title="The name of the initial administrator group">{{ Html::icon('fa-users') }}</span>
				</div>
			</div>

			<div class="row collapse {{ $errors->has('v') ? 'error' : '' }}">
				<div class="small-12 columns">
					{{ Form::textarea('description', 'We gotta power.', array( 'placeholder' => 'Group Name')) }}
				</div>
			</div>

			{{ Form::submit('Submit', array( 'class' => 'small  button' )) }}

		{{ Form::close() }}
	</div>
	@stop