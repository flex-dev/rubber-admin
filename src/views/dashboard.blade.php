@extends('rubber::layouts.detail')
	@section('content_header')
		<h2>Dashboard: <small>{{{ $user->full_name_and_email }}}</small></h2>
	@stop

	@section('detail')
		<div class="row">
			@include('rubber::users.partials.info', ['user' => $user, 'personal' => true])
			<div class="large-9 columns">
				@include('rubber::users.notifications.list', ['notifications' => $user->notifications])
			</div>
		</div>
	@stop