<?php namespace Rubber\Admin;
	use Sentry;
	class Chronicle{

		protected $user = null;


		public function __construct(){
			$this->user = Sentry::getUser();;
		}

		public function write($message, $model = null){
			$access = $this->user->access()->orderBy('id', 'desc')->first();
			if(empty($access)){
				$access = new UserAccess;
				$access->save();
			}
			$history = new UserHistory;
			$history->content = $message;
			$history->access()->associate($access);

			if(!empty($model)){
				if($model instanceOf  \Illuminate\Support\Contracts\JsonableInterface){
					$history->data = $model->toJson();
				}
			}

			$history->save();
			return;
		}
	}
?>