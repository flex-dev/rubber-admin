var Rubber = Rubber || {};

Rubber.init = function(){
	Rubber.form.listener();
	Rubber.previewer();
};

Rubber.previewer = function(){
	$("body").on("click", ".previewer", function(ev){
		ev.preventDefault();
		var element = $(this);
		var url = element.attr('href') || element.attr('src');
		var img = $("<img>").prop('src', url);
		$("#previewModel").empty().append(img);
		$("#previewModel").foundation('reveal')
			.foundation('reveal', 'open');
	});
}

Rubber.form = {};

Rubber.form.listener = function(){
	$("select.chosen").chosen();

	$('body').on('submit', '.delete-form', function(ev){
		ev.preventDefault();
		var form = $(this);
		$("#deleteConfirm").one('click',  function(){
			$('body').off('submit');
           		form.submit();
		});
		$("#deleteCancel").one('click',  function(){
			$("#deleteFormModal").foundation('reveal', 'close');
		});
		$("#deleteFormModal").foundation('reveal', 'open');
	});

	$('body').on('submit', '.restore-form', function(ev){
		ev.preventDefault();
		var form = $(this);
		$("#restoreConfirm").one('click',  function(){
			form.unbind('submit').submit();
		});
		$("#restoreCancel").one('click',  function(){
			$("#restoreFormModal").foundation('reveal', 'close');
		});
		$("#restoreFormModal").foundation('reveal', 'open');
	});

	// Custom file input, with preview & remove button
	$('body').on('change', '.filePlaceholder input[type=file]', function(ev){
		var f = ev.target.files[0];
		var text = escape(f.name) +  '(' + (f.type || 'n/a') + ') - '+ f.size/1000 + ' kilobytes';
		var placeholder = $(this).parents('.filePlaceholder').find('.uploadPlaceholder');
		if(!placeholder.data('original')){
			placeholder.data('original', placeholder.val());
		}
		placeholder.val(text);

		var img = $(this).parents('.filePlaceholder').find('img.filePreview');
		if(f.type.indexOf('image') != -1){
			img.show().siblings().hide();
			readURL(this, img)
			img.unbind('click').on('click', function(ev){
				$("#previewModel").html(img.clone()).foundation('reveal', 'open');
			});
		}
		else{
			img.hide().siblings().show();
		}

		var fileRemove = $(this).siblings('.fileRemove');
		fileRemove.one('click', function(ev){
			ev.preventDefault();
			var input = $(this).parents('.filePlaceholder').find('input[type=file]');
			var placeholder = $(this).parents('.filePlaceholder').find('.uploadPlaceholder');
			input.replaceWith(input.clone(true));
			placeholder.val(placeholder.data('original'));
			$(this).hide();

			img.hide().siblings().show();

			return;
		}).show();

		return;
	});

	// Toggle  all permission in the nearest table
	$("body").on('change', '.toggleAll', function(ev){
		var toggler = $(this);
		var val = this.checked;
		var permissions = $(this).closest('.content')
			.find('table')
			.find('input[name^=permissions]');
		console.dir(val);
		permissions.each(function(idx, permission){
			permission.checked = val;
		});
	});

}

readURL = function(input, target)
{
	if (input.files && input.files[0])
	{
		//var target = $(input).attr('rel');
		var reader = new FileReader();
		reader.onload = function(e)
	{
		$(target).attr('data-src',"rendered");
		$(target).prop('src', e.target.result);
	}
		reader.readAsDataURL(input.files[0]);
	}
}

Rubber.factory = {};

$(function(){
	Rubber.init();
});
